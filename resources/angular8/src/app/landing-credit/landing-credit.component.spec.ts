import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingCreditComponent } from './landing-credit.component';

describe('LandingCreditComponent', () => {
  let component: LandingCreditComponent;
  let fixture: ComponentFixture<LandingCreditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingCreditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingCreditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
