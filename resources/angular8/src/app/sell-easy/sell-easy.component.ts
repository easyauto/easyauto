import { Component, OnInit, ViewChildren, ElementRef, QueryList, ViewChild } from '@angular/core';
import { DataBaseService } from "../data-base.service"
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import swal from 'sweetalert2';
import { FormGroup,FormBuilder,Validators,FormArray, FormControl, FormGroupName } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LoginComponent } from './../menu/login/login.component';
import { AppValues } from './../shared/data/app-values';
import { functions } from '../shared/data/functions';

export interface dataImage{src:string,loading?:boolean,name?:string,valid?:boolean,id:number,main:number}

@Component({
  selector: 'app-sell-easy',
  templateUrl: './sell-easy.component.html',
  styleUrls: ['./sell-easy.component.css']
})
export class SellEasyComponent implements OnInit {
  ComboBrands:Array<any>
  ComboModels:Array<any>
  ComboType:Array<any>
  ArrayModels:Array<any>;
  ComboDisplacement:Array<any>;
  Regions:Array<any>
  public ArrayImages:Array<dataImage>
  Year:Array<any>
  spinner:boolean=false;
  num_owners:Array<any>
  transmission:Array<any>
  bodywork:Array<any>
  direction:Array<any>
  roof:Array<any>
  doors:Array<any>
  airbag:Array<any>
  traction:Array<any>
  files:FileList;
  FormPublications:FormGroup;
  FormPublications2:FormGroup;
  FormPublications3:FormGroup;
  IdGenerated:string=null;
  imageMainNumber:number=0;
  form1:boolean=false;
  form2:boolean=false;
  form3:boolean=false;
  loading:boolean=false;
  @ViewChild('brand', {static: false}) brand: any;
  @ViewChild('model', {static: false}) model: ElementRef;
  @ViewChild('mileage', {static: false}) mileage: ElementRef;
  @ViewChild('price', {static: false}) price: ElementRef;
  @ViewChild('year', {static: false}) year: ElementRef;
  @ViewChild('patent', {static: false}) patent: ElementRef;
  constructor(public AppValues:AppValues,private DB:DataBaseService, private router: Router,public route:ActivatedRoute,private formBuilder: FormBuilder,public dialog: MatDialog, public functions:functions) {
    this.ComboBrands=[];
    this.ComboModels=[];
    this.ComboType = [];
    this.ArrayImages = [];
    this.ComboDisplacement=[];
  }

  ngOnInit() {
    this.IdGenerated = this.makeid(12);
    this.ComboDisplacement=[];
    this.ComboDisplacement.push({id:1300,name:1300+' c.c.'});
    this.ComboDisplacement.push({id:1400,name:1400+' c.c.'});
    this.ComboDisplacement.push({id:1500,name:1500+' c.c.'});
    this.ComboDisplacement.push({id:1600,name:1600+' c.c.'});
    this.ComboDisplacement.push({id:1700,name:1700+' c.c.'});
    this.ComboDisplacement.push({id:1800,name:1800+' c.c.'});
    this.ComboDisplacement.push({id:2000,name:2000+' c.c.'});
    this.ComboDisplacement.push({id:2500,name:2500+' c.c.'});
    this.ComboDisplacement.push({id:'',name:'_______________'});
    for (let i = 50; i < 18000; i=i+50) {
      this.ComboDisplacement.push({id:i,name:i+' c.c.'})
    }

    this.DB.PublicationsGet().subscribe((data:any)=>{
      this.ComboBrands = data.brands.sort((a, b)=>(a.description > b.description)?1:-1);
      this.ArrayModels =  data.models.sort((a, b)=>(a.description > b.description)?1:-1);
      this.Regions =  data.regions
      this.ComboType = data.type_publications.map((i) => { i.descriptionFull = i.description + ' - $' + i.price; return i; })
      this.route.queryParams.subscribe((params:any)=>{
        if(params.publication){
          this.DB.PublicationsGetById(params.publication).subscribe((data:any)=>{
            this.ArrayImages = data.publication.photos.map((i) => { i.src = "http://localhost:8000"+i.route; i.valid = true; i.name = i.route.split("photos_vehicles/")[1];return i; });
            console.log("this.ArrayImages",this.ArrayImages);

            this.FormPublications.controls.brand.setValue(this.ArrayModels.filter((m:any)=>m.id == data.publication.id_modul)[0].id_brand)
            this.Search_Models()
            this.FormPublications.patchValue({
              id:data.publication.id,
              model: data.publication.id_modul,
              //type_publications:data.publication.id_type_publications,
              mileage:data.publication.mileage,
              price:this.functions.formatNumberToAmount(data.publication.price),
              year:data.publication.year,
              images:'',
              files:'',
              imagesData:null,
              id_user:data.publication.id_user,
              regions:data.publication.id_regions,
              description:data.publication.description,

              up_glasses:data.publication.up_glasses,
              mirrors:data.publication.mirrors,
              close_door:data.publication.close_door,
              air:data.publication.air,
              weather:data.publication.weather,
              abs:data.publication.abs,
              radio:data.publication.radio,
              tires:data.publication.tires,
              debt:data.publication.debt,
              changeable:data.publication.changeable,
              maintenance_agency:data.publication.maintenance_agency,

              patent:data.publication.patent,
              version:data.publication.version,
              num_owners:data.publication.num_owners,
              bodywork:(data.publication.bodywork!==null)?data.publication.bodywork.toString():null,
              gas:(data.publication.gas!==null)?data.publication.gas.toString():null,
              transmission:(data.publication.transmission!==null)?data.publication.bodywork.toString():null,
              displacement:(data.publication.displacement!==null)?data.publication.bodywork.toString():null,
              hp:data.publication.hp,
              direction:(data.publication.direction!==null)?data.publication.bodywork.toString():null,
              roof:(data.publication.roof!==null)?data.publication.bodywork.toString():null,
              doors:(data.publication.doors!==null)?data.publication.bodywork.toString():null,
              airbag:(data.publication.airbag!==null)?data.publication.bodywork.toString():null,
              color:data.publication.color,
              traction:(data.publication.traction!==null)?data.publication.bodywork.toString():null,
            })
            this.FormPublications2.patchValue({
              name_pub:data.publication.name_pub,
              rut_pub:data.publication.rut_pub,
              name_owner:data.publication.name_owner,
              rut_owner:data.publication.rut_owner,
              phone_pub:data.publication.phone_pub,
              commune:data.publication.commune,
              regions:data.publication.id_regions,
            })
          })
        }else{
          this.ArrayImages=[]
          this.FormPublications.reset()
        }
      })
    })
    this.FormPublications2 = this.formBuilder.group({
      regions:[null, [Validators.required]],
      name_pub:[null, [Validators.required]],
      rut_pub:[null, [Validators.required]],
      // name_owner:[null, [Validators.required]],
      // rut_owner:[null, [Validators.required]],
      phone_pub:[null, [Validators.required]],
      commune:[null, [Validators.required]],
    });
    this.FormPublications3 = this.formBuilder.group({
      type_publications:[null, [Validators.required]],
    });
    this.FormPublications = this.formBuilder.group({
      id:null,
      files:null,
      brand: [null, [Validators.required]],
      model: [null, [Validators.required]],
      mileage:[null, [Validators.required]],
      price:[null, [Validators.required]],
      year:[null, [Validators.required]],
      imagesData:[],
      id_user:null,
      description:null,
      up_glasses:false,
      mirrors:false,
      close_door:false,
      air:false,
      weather:false,
      abs:false,
      radio:false,
      tires:false,
      debt:false,
      changeable:false,
      maintenance_agency:false,

      patent:[null, [Validators.required]],
      version:null,
      gas:null,
      num_owners:null,
      bodywork:null,
      transmission:null,
      displacement:null,
      hp:null,
      direction:null,
      roof:null,
      doors:null,
      airbag:null,
      color:null,
      traction:null,
    });
  }
  validateForm(form:FormGroup,formNumber:number){
    if(form.invalid){
      switch (formNumber) {
        case 1:
          this.form1=true;
        break;
        case 2:
          this.form2=true;
        break;
        case 3:
          this.form3=true;
        break;
      }
    }
  }
  makeid(length:number) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }
  createImage():FormGroup{
    return this.formBuilder.group({
      src:''
    })
  }
  Search_Models(){
    this.FormPublications.controls.model.reset()
    this.ComboModels = this.ArrayModels.filter(model=>model.id_brand == this.FormPublications.controls.brand.value)
  }

  Upload_Image(evt:any){
    this.files = evt.target.files; // FileList object
    for (let i = 0; i < this.files.length; i++) {
      if(this.ArrayImages.length+i<20){
        this.ShowImages(this.files[i]);
      }else{
        swal({text:"No puedes subir mas de 20 imagenes!",type:"error"}).then(()=>{return});
      }
    }
    setTimeout(() => {
      for (let j = 0; j < this.ArrayImages.length; j++) {
        const img = this.ArrayImages[j];
        var block = img.src.split(";");
        var contentType = block[0].split(":")[1];
        var realData = block[1].split(",")[1];
        let file = new File([this.b64toBlob(realData,contentType)],"cache"+j+"."+contentType.split("/")[1]);
        this.Upload_to_server(file,this.IdGenerated,j.toString());
      }
    }, 1500);
  }
  b64toBlob(b64Data:string, contentType:string, sliceSize?:any) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }
  ShowImages(file:File){
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.ArrayImages[this.ArrayImages.length] = {src:fileReader.result.toString(),loading:true,valid:false,id:null,main:0};
    }
    fileReader.readAsDataURL(file);
  }
  Upload_to_server(file:File,id:string,number:string){
    let post = new FormData();
    post.append("file",file,file.name);
    post.append("id",id);
    post.append("number",number.toString());
    this.DB.SaveImage(post).subscribe((response:any)=>{
    console.log("SaveImage",response);
    if(this.ArrayImages[number])this.ArrayImages[number].loading = false;
      if(response.status == "success"){
        if(this.ArrayImages[number]){
          this.ArrayImages[number].name = response.name;
          this.ArrayImages[number].valid = true;
        }
      }else{
        this.DeleteImage(number);
      }
    },error=>{
      swal({text:"Error al subir imagen"})
      this.DeleteImage(number);
      console.log(error);
    })
  }
  DeleteImage(i){
    this.ArrayImages.splice(i, 1);
  }
  onSubmitFormPublications(e){
    if(this.FormPublications2.invalid){this.form2=true;return false;}
    if(this.FormPublications.invalid){swal({text:"Debes completar los campos requeridos"});return false;};
    if(this.ArrayImages.length==0){swal({text:"Debes incluir almenos una imagen"});return false;};
    swal({text:'Cargando datos...',onBeforeOpen: () => {swal.showLoading();}});
    if(this.ArrayImages.length<=20){
      this.loading = true;
      if(this.DB.UserLogIn==true){
        this.FormPublications.patchValue({
          imagesData: this.ArrayImages.map(a=>a.name),
          id_user: this.DB.UserLogInData.user.id
        });
        let body = Object.assign(this.FormPublications.value, this.FormPublications2.value, this.FormPublications3.value);
        this.DB.PublicationsPost(body).subscribe((data:any)=>{
          swal.close();
          if(data.status=='success'){
            let body = {
              imagesData:this.ArrayImages.map(a=>a.name),
              id_publication:data.id_publication,
              main:this.imageMainNumber,
              method:"add"
            }
            this.DB.ChangeImagesNameCache(body).subscribe((resp:any)=>{
              console.log("ChangeImagesNameCache (add)::",resp);
              this.loading = false;
              if(data.status=='success'){
                this.router.navigate(['/promotions'],{queryParams:{publication:data.id_publication}});
              }else{
                swal({text:"Error al guardar las imagenes"});
              }
            },error=>{swal({text:"Error images 21",type:'error'});console.log(error);this.loading = false;})
          }else{
            swal({text:'En estos momentos no podremos procesar su peticion',type:'info'});
          }
        },error=>{
          swal({text:'En estos momentos no podremos procesar su peticion',type:'error'});
          console.log(error);
          this.loading = false;
        })
      }else{
        swal({text:'Debes loguearte primero'}).then(()=>{
          this.OpenDalogLogin();this.loading = false;
        });
      }
    }else{
        swal({text:'Maximo de 20 fotos'});
    }
  }
  validateFormGroup(form:FormGroup){
    let keys = Object.keys(form.value);
    let first = true;
    keys.forEach(key => {
      if(form.controls[key].errors){
        if(first == true){
          console.log(this[key].focusEvent());
          // this[key]._element.nativeElement.focus();
          first=false;
        }
      }
    });
  }
  UpdatePublication(e){
    if(this.FormPublications2.invalid){this.form2=true; this.validateFormGroup(this.FormPublications2) ;return false;}
    if(this.FormPublications.invalid){swal({text:"Debes completar los campos requeridos"});this.validateFormGroup(this.FormPublications) ;return false;};
    if(this.ArrayImages.length==0){swal({text:"Debes incluir almenos una imagen"});return false;};
    swal({text:'Cargando datos...',onBeforeOpen: () => {swal.showLoading();}});
    if(this.ArrayImages.length<=20){
      this.loading = true;
      this.FormPublications.patchValue({
        imagesData: this.ArrayImages.map(a=>a.name),
        id_user: this.DB.UserLogInData.user.id
      });
      let body = Object.assign(this.FormPublications.value, this.FormPublications2.value, this.FormPublications3.value);
      this.DB.PublicationsPutById(this.FormPublications.value.id,body).subscribe((data:any)=>{
        // swal.close();
        if(data.status=='success'){
          let body = {
            imagesData:this.ArrayImages.map(a=>a.name),
            id_publication:this.FormPublications.value.id,
            main:this.imageMainNumber,
            method:"update"
          }
          this.DB.ChangeImagesNameCache(body).subscribe((resp:any)=>{
            console.log("ChangeImagesNameCache (update)::",resp);
            this.loading = false;
            if(resp.status=='success'){
              swal({text:'Publicación Modificada!',type:'success'});
              this.router.navigate(['/publications']);
            }else{
              swal({text:"Error al guardar las imagenes"});
            }
          },error=>{swal({text:"Error images 21",type:'error'});console.log(error);this.loading = false;})
        }else{
          swal({text:'En estos momentos no podremos procesar su peticion',type:'info'});
        }
      },error=>{
        swal.close();
        console.log("algun error",error);
        this.loading = false;
      })
    }else{
      swal({text:'maximo de 20 fotos',type:'error'});
    }
  }
  OpenDalogLogin(){
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '300px',
      data: {}
    });
  }
  formatDisplacement(){
    this.FormPublications.patchValue({
      displacement:(this.FormPublications.value.displacement).toFixed(2)
    })
  }
  toRouter(m:string){
    console.log(m);

  }
  setImageToMain(i:number){
    for (let j = 0; j < this.ArrayImages.length; j++) {
      const element = this.ArrayImages[j];
      if(i==j){
        this.ArrayImages[j].main=1;
        this.imageMainNumber = j;
      }else{
        this.ArrayImages[j].main=0;
      }
    }
  }
}
