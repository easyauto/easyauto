import { DataBaseService } from "../data-base.service"
import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import swal from "sweetalert2";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  TextMenu:Object
  Regions:Array<any>
  public Citys:Array<any>
  public Communes:Array<any>
  public FormRegister: FormGroup
  //public FormPublications: FormGroup
  
  constructor(public DB: DataBaseService,private formBuilder: FormBuilder,private router: Router) { 
    this.TextMenu =
    {
      "es_CL":{
        publications:"Publicaciones",
        profile:"Editar Perfil",
        greet:"Saludo en español",
        name:"Nombre",
        lastname1:"Apellido Paterno",
        lastname2:"Apellido Materno",
        phone:"Movil",
        vemail:"Formato Email Invalido",
        vemail2:"Email Requerido",
        vemail3:"Email",
        vpassword1:"New Password ",
        vpassword12:"Password Requerido",
        button:"Guardar Cambios",
        birthdate:"Fecha Nacimiento",
        rut:"RUT",
        region:"Seleccione Region",
        changesSuccess:"Cambios Guardados",
        changesError:"Error al guardar",
      },
      "en_US":{
        publications:"Publicaciones",
        profile:"Editar Perfil",
        greet:"greeting in english",
        name:"Name",
        vemail:"Format Email Invalid!",
        vemail2:"Email Requered",
        vemail3:"Email",
        vpassword1:"Password ",
        vpassword12:"Password Requered",
        button:"Save Changes"
      }
    }
    this.Regions=[]
  }

  ngOnInit() {
    let user = JSON.parse(localStorage.getItem("user"))
    this.FormRegister = this.formBuilder.group({
      name:[user.name,[Validators.required]],
      email: [user.email, [Validators.email,Validators.required]],
      birthdate:user.birthdate,
      rut:user.rut,
      phone:user.phone,
      lastname1:user.lastname1,
      lastname2:user.lastname2,
      regions:user.id_region,
      city:user.id_city,
      commune:user.id_commune,
    });
  }
  UpdateUser(){
    swal.showLoading();
    this.DB.UpdateUser(this.FormRegister.value).subscribe((data:any)=>{
      if(data.status=='success'){
        swal({text:this.TextMenu[this.DB.lang].changesSuccess})
        this.DB.UserLogInData.user=data.user
        localStorage.setItem('user',JSON.stringify(data.user));
        this.ngOnInit()
      }else{
        swal({text:this.TextMenu[this.DB.lang].changesError})
      }
    },error=>{swal.close();console.log(error);})
  }
  
}
