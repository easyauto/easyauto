import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProfileComponent } from './profile/profile.component';
import { SellEasyComponent } from './sell-easy/sell-easy.component';
import { SearchComponent } from './search/search.component';
import { CreditComponent } from './credit/credit.component';
import { InsuranceComponent } from './insurance/insurance.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { FaqComponent } from './faq/faq.component';
import { AdministratorComponent } from './administrator/administrator.component';
import { PublicationsComponent } from './publications/publications.component';
import { MessagesComponent } from './messages/messages.component';
import { ViewComponent } from './publications/view/view.component';
import { PromotionsComponent } from './promotions/promotions.component';
import { LandingCreditComponent } from './landing-credit/landing-credit.component';
import { ShortCreditComponent } from './short-credit/short-credit.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { UpgradePublicationComponent } from './upgrade-publication/upgrade-publication.component';
import { ErrorComponent } from './upgrade-publication/error/error.component';
import { SuccessComponent } from './upgrade-publication/success/success.component';
import { AuthGuard } from './shared/auth.guard';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'search', component: SearchComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'publications', component: PublicationsComponent},
  {path: 'view_publication', component: ViewComponent},
  {path: 'messages', component: MessagesComponent,canActivate:[AuthGuard]},
  {path: 'administrator', component: AdministratorComponent,canActivate:[AuthGuard]},
  {path: 'sell_easy', component: SellEasyComponent},
  {path: 'creditoautomotriz', component: ShortCreditComponent},
  {path: 'credito-automotriz', component: CreditComponent},
  {path: 'insurance', component: InsuranceComponent},
  {path: 'about-us', component: AboutUsComponent},
  {path: 'faq', component: FaqComponent},
  {path: 'promotions', component: PromotionsComponent},
  {path: 'promocion-credito-automotriz', component: LandingCreditComponent},
  {path: 'terms-and-conditions', component: TermsAndConditionsComponent},
  {path: 'upgrade-publication', component: UpgradePublicationComponent,canActivate:[AuthGuard]},
  {path: 'khipu-cancel/:id', component: ErrorComponent},
  {path: 'khipu-success/:id', component: SuccessComponent},
  {path: '**', component: PageNotFoundComponent}
]

@NgModule({
  imports:[RouterModule.forRoot(routes)],
  providers:[
    AuthGuard
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }
