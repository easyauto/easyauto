import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';


@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.component.html',
  styleUrls: ['./promotions.component.css']
})
export class PromotionsComponent implements OnInit {
  textUrl:string="";
  var1:boolean=true;
  var2:boolean=true;
  id_pub:string="";
  constructor(public route:ActivatedRoute, private _location: Location) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params:any)=>{
      if(params.publication){
        this.id_pub = params.publication;
      }
    });
    this.textUrl = "Hola, necesito asesoria tecnica con un experto automotriz, puede checar mi publicacion?  https://www.easyautos.cl/view_publication?publication=" +this.id_pub;
  }

  /*find(){
    if(this.var1 && !this.var2){
      this.textUrl = encodeURI("Solicito información para vender mi vehículo por consignación virtual");
    }else if(this.var2 && !this.var1){
      this.textUrl = encodeURI("Solicito información para vender mi vehículo por compra inmediata");
    }else if(this.var2 && this.var1 || !this.var2 && !this.var1){
      this.textUrl = encodeURI("Solicito información para vender mi vehículo por compra inmediata y por consignación virtual");
    }
  }*/

  /*backClicked() {
      this._location.back(); 
   }*/
}
