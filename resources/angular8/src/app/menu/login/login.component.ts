import { Component, OnInit, Inject, isDevMode } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { DataBaseService } from "../../data-base.service";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RegisterComponent } from './../register/register.component'
import swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public FormLogin: FormGroup
  TextMenu:Object
  constructor(public DB: DataBaseService, private router: Router,private route:ActivatedRoute,private formBuilder: FormBuilder,public dialogRef: MatDialogRef<LoginComponent>, @Inject(MAT_DIALOG_DATA) public data: any,public DialogRef:MatDialogRef<LoginComponent>,public dialog:MatDialog) {
    this.TextMenu =
    {
      "es_CL":{
        title:"Iniciar Sesión",
        vemail:"Formato Email Invalido",
        vemail2:"Email Requerido",
        vemail3:"Email",
        vpassword:"Password ",
        vpassword2:"Password Requerido",
        button:"Iniciar Sesión",

        msg_success:"Inicio de sesión correcto!",
        msg_invalid_credential:"Credenciales Invalidas!",
        msg_error_user:"Email Invalido!",
        msg_error_server:"No se pudo conectar al servidor"
      },
      "en_US":{
        title:"Log In",
        vemail:"Format Email Invalid!",
        vemail2:"Email Requered",
        vemail3:"Email",
        vpassword:"Password ",
        vpassword2:"Password Requered",
        button:"Log In",

        msg_success:"Successful session!",
        msg_invalid_credential:"Credencials Invalid!",
        msg_error_user:"Email Invalid!",
        msg_error_server:"Could not connect to server"
      }
    }
  }

  ngOnInit() {
    this.FormLogin = this.formBuilder.group({
      email: [isDevMode()?"ing.eloyprieto@gmail.com":null, [Validators.email,Validators.required]],
      password: [isDevMode()?"spy123":null,[Validators.required]]
    });
  }
  login(){
    swal.showLoading();
    this.DB.LogInPost(this.FormLogin.value).subscribe((data:any)=>{
      if (data.status=="success"){
        this.DB.UserLogInData = data.data
        this.DB.UserLogIn = true
        localStorage.setItem("token",JSON.stringify(data.data.token))
        localStorage.setItem("user",JSON.stringify(data.data.user))
        this.dialogRef.close();
        swal({text:this.TextMenu[this.DB.lang].msg_success,type:'success'});
      }else{
        swal({text:this.TextMenu[this.DB.lang].msg_error_user,type:'error'});
      }
    },error=>{
      switch (error.status) {
        case 401:
          swal({text:this.TextMenu[this.DB.lang].msg_invalid_credential,type:'error'});
        break;
        case 500:
          swal({text:this.TextMenu[this.DB.lang].msg_error_server,type:'error'});
        break;
        default:
          console.log("error",error.error);
        break;
      }
    })
  }
  OpenModalRegister(){
    this.DialogRef.close()
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '300px',
      data: {}
    });
  }
}
