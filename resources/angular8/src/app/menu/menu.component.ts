import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { DataBaseService } from "../data-base.service"
import { Router } from "@angular/router";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  TextMenu:object
  lang:string
  dialogRefLogin:boolean=false;
  dialogRefRegister:boolean=false;
  userName = (localStorage.getItem("user"))?JSON.parse(localStorage.getItem("user")).name:'';
  constructor(public ruta:ActivatedRoute, public DB:DataBaseService,public dialog: MatDialog,private router: Router) {
    this.TextMenu =
    {
      "es_CL":{
        banner:"La forma mas facil de comprar y vender tu auto!",
        credit:"Credito",
        insurance:"Seguro",
        new_cars:"Autos Nuevos",
        sell_easy:"Vende Facil",
        login:"Ingresa",
        register:"Registrate",
        profile:"Perfil",
        logout:"Salir"
      },
      "en_US":{
        banner:"The easiest way to buy and sell your car!",
        credit:"Credit",
        insurance:"Insurance",
        new_cars:"New Cars",
        sell_easy:"Sell Easy",
        login:"LogIn",
        register:"Sign up",
        profile:"Profile",
        logout:"Logout"
      }
    }
  }

  ngOnInit() {
    let token = JSON.parse(localStorage.getItem("token"))
    if(token!=null){
      this.DB.ValidateToken().subscribe((data:any)=>{
        if(data.status=="success"){
          this.DB.UserLogInData.user = JSON.parse(localStorage.getItem("user"))
          this.DB.UserLogInData.token = JSON.parse(localStorage.getItem("token"))
          this.DB.UserLogIn = true
        }else{
          this.DB.logout()
        }
      },error=>this.DB.ErrorFn(error))
    }
  }
  ChangeLanguage(lang){
    this.DB.lang = lang
  }
  OpenDalogLogin(){
    if(this.dialogRefLogin!=true){
      const dialogRefL = this.dialog.open(LoginComponent, {
        width: '300px',
        data: {}
      });
        this.dialogRefLogin=true
      dialogRefL.afterClosed().subscribe(result => {
        this.dialogRefLogin=false
      });
    }
  }
  OpenDalogRegister(){
    if(this.dialogRefRegister!=true){
      const dialogRefR = this.dialog.open(RegisterComponent, {
        width: '300px',
        data: {}
      });
        this.dialogRefRegister=true
      dialogRefR.afterClosed().subscribe(result => {
        this.dialogRefRegister=false
      });
    }
  }
  toggleMenu(){
    var menu = document.getElementById('side-menu');
    menu.classList.toggle("active");
  }
}
