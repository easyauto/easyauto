import { Component, OnInit,Inject } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { DataBaseService } from "../../data-base.service"

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public FormRegister: FormGroup
  ActivatorButton:boolean
  TextMenu:Object
  constructor(public DB: DataBaseService, private router: Router,private route:ActivatedRoute,private formBuilder: FormBuilder,public dialogRef: MatDialogRef<RegisterComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { 
    this.TextMenu =
    {
      "es_CL":{
        title:"Registrate!",
        name:"Nombre",
        vemail:"Formato Email Invalido",
        vemail2:"Email Requerido",
        vemail3:"Email",
        vpassword1:"Password ",
        vpassword12:"Password Requerido",
        vpassword2:"Password ",
        vpassword22:"Password Requerido",
        button:"Registrar",
        validator_password_same:"Las contraseñas son diferentes",

        msg_success:"Registro Exitoso!",
        msg_error:"Registro Fallido!"
      },
      "en_US":{
        title:"Sign up",
        name:"Name",
        vemail:"Format Email Invalid!",
        vemail2:"Email Requered",
        vemail3:"Email",
        vpassword1:"Password ",
        vpassword12:"Password Requered",
        vpassword2:"Password ",
        vpassword22:"Password Requered",
        button:"Register",
        validator_password_same:"The passwords are diferent",

        msg_success:"Successful registration!",
        msg_error:"Sign up failed!"
      }
    }
  }

  KeyUpValidator(){
    this.ActivatorButton = (this.FormRegister.get('password').value == this.FormRegister.get('password2').value)?false:true;
  }
  
  ngOnInit() {
    this.FormRegister = this.formBuilder.group({
      name:['',[Validators.required]],
      email: ['', [Validators.email,Validators.required]],
      password: ['',[Validators.required]],
      password2: ['',[Validators.required]],
    });
  }
  register(){
    swal.showLoading();
    this.DB.RegisterPost(this.FormRegister.value).subscribe((data:any)=>{
      this.login();
    },error=>{
      swal({text:this.TextMenu[this.DB.lang].msg_error,type:'error'});
    })
  }
  login(){
    this.DB.LogInPost({email:this.FormRegister.value.email,password:this.FormRegister.value.password}).subscribe((data:any)=>{
      if (data.status=="success"){
        this.DB.UserLogInData = data.data
        this.DB.UserLogIn = true
        localStorage.setItem("token",JSON.stringify(data.data.token))
        localStorage.setItem("user",JSON.stringify(data.data.user))
        this.dialogRef.close();
        swal({text:this.TextMenu[this.DB.lang].msg_success,type:'success'});
      }else{
        swal({text:this.TextMenu[this.DB.lang].msg_error_user,type:'error'});
      }
    },error=>{
      switch (error.status) {
        case 401:
          swal({text:this.TextMenu[this.DB.lang].msg_invalid_credential,type:'error'});
        break;
        case 500:
          swal({text:this.TextMenu[this.DB.lang].msg_error_server,type:'error'});
        break;
        default:
          console.log("error",error.error);
        break;
      }
    })
  }
}
