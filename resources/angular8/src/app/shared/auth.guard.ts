import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { DataBaseService } from '../data-base.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(public DataBaseService:DataBaseService, public router: Router) {}
  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let result = new Subject<boolean>();
    this.DataBaseService.ValidateToken().subscribe((value)=>{
      if(value){
        this.DataBaseService.UserLogIn = true;
        this.DataBaseService.UserLogInData = JSON.parse(localStorage.getItem("user"));
        result.next(true);
      }else{
        result.next(false);this.DataBaseService.logout();
      }
    });
    return result.asObservable();
  }
}