import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateplus'
})
export class DateplusPipe implements PipeTransform {

  transform(value: any = "", args?: any): any {
    let r:string = value.toString().substr(0,10)
    let result = r.split('-')
    return (result[2]+'/'+result[1]+'/'+result[0]).toString()
  }

}
