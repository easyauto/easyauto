import { Pipe, PipeTransform } from '@angular/core';
import { AppValues } from './../data/app-values';

@Pipe({
  name: 'transmissionplus'
})
export class TransmissionplusPipe implements PipeTransform {
  
  constructor(private AppValues:AppValues) { }
  transform(value: any, args?: any): any {
    let r = this.AppValues.transmission.filter(trans=>trans.id == value)
    return r.length == 0 ? r : r[0].short_name;
  }

}
