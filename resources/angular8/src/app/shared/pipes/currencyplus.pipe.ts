import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'currencyplus'
})
export class CurrencyplusPipe implements PipeTransform {

  transform(value: any = "", args?: any): any {
    let n = String(value).toString().replace(/\D/g, "");
    return n === '' ? n : '$ '+Number(n).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  }

}
