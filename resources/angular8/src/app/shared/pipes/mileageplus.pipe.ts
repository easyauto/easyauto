import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mileageplus'
})
export class MileageplusPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let n = String(value).replace(/\D/g, "");
    return n === '' ? n : Number(n).toLocaleString();
  }

}
