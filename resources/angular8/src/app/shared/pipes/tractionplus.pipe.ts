import { Pipe, PipeTransform } from '@angular/core';
import { AppValues } from './../data/app-values';
@Pipe({
  name: 'tractionplus'
})
export class TractionplusPipe implements PipeTransform {

  constructor(private AppValues:AppValues) { }
  transform(value: any, args?: any): any {
    let r = this.AppValues.traction.filter(trans=>trans.id == value)
    return r.length == 0 ? r : r[0].name;
  }

}
