import { Pipe, PipeTransform } from '@angular/core';
import { AppValues } from './../data/app-values';
@Pipe({
  name: 'doorsplus'
})
export class DoorsplusPipe implements PipeTransform {

  constructor(private AppValues:AppValues) { }
  transform(value: any, args?: any): any {
    let r = this.AppValues.doors.filter(trans=>trans.id == value)
    return r.length == 0 ? r : r[0].name;
  }

}
