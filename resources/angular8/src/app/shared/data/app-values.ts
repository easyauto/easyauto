export class AppValues {
    static convenios =  [
        {url:"assets/images/logos-marcas/peugeot.png"},
        {url:"assets/images/logos-marcas/chevrolet.png"},
        {url:"assets/images/logos-marcas/toyota.png"},
        {url:"assets/images/logos-marcas/hyundai.png"},
        {url:"assets/images/logos-marcas/nissan.png"},
        {url:"assets/images/logos-marcas/mercedes-benz.png"},
        {url:"assets/images/logos-marcas/audi.png"},
        {url:"assets/images/logos-marcas/jeep.png"},
        {url:"assets/images/logos-marcas/bmw.png"}
    ];
    gas = [
        {id:"1",name:"Bencina"},
        {id:"2",name:"Diesel (petróleo)"},
        {id:"3",name:"Eléctrico"},
        {id:"4",name:"Gas"},
        {id:"5",name:"Híbrido"},
        {id:"6",name:"Otros"},
    ];
    num_owners = [
        {id:"1",name:"1"},
        {id:"2",name:"2"},
        {id:"3",name:"3"},
        {id:"4",name:"4 o mas"}
    ];
    transmission = [
        {id:"1",name:"Automatica",short_name:"AUT"},
        {id:"2",name:"Manual",short_name:"MEC"},
        {id:"3",name:"Triptonic",short_name:"TRI"},
        {id:"4",name:"Paddle Shift",short_name:"PSH"}
    ];
    bodywork = [
        {id:"1",name:"Sedan"},
        {id:"2",name:"Suv"},
        {id:"3",name:"Convertible"},
        {id:"4",name:"Pickup"},
        {id:"5",name:"Mini van"},
        {id:"6",name:"Hachback"},
        {id:"7",name:"Coupe"},
        {id:"7",name:"Station wagon"}
    ];
    direction = [
        {id:"1",name:"Hidráulico"},
        {id:"2",name:"Mecanica"},
        {id:"3",name:"Asistida"}
    ];
    roof = [
        {id:"1",name:"Ninguno"},
        {id:"2",name:"Eléctrico"},
        {id:"2",name:"Manual"},
        {id:"2",name:"Panorámico"},
    ];
    doors = [
        {id:"1",name:"2"},
        {id:"2",name:"3"},
        {id:"3",name:"4"},
        {id:"4",name:"5 o mas"}
    ];
    airbag = [
        {id:"1",name:"1"},
        {id:"2",name:"2"},
        {id:"3",name:"3"},
        {id:"4",name:"4"},
        {id:"5",name:"5"},
        {id:"6",name:"6"},
        {id:"7",name:"7"},
        {id:"8",name:"8 o mas"}
    ];
    traction = [
        {id:"1",name:"4x2"},
        {id:"2",name:"4x4"},
        {id:"3",name:"2WD"},
        {id:"4",name:"4WD"},
        {id:"5",name:"AWD"},
        {id:"6",name:"Otro"}
    ];
    Year = this.GetYear();
    GetYear(){
        let Year=[]
        let ano = new Date().getFullYear()
        for (let index = ano; index >= 1940; index--) {
            Year.push({id:index,name:index})
        }
        return Year;
    }
}
