export class functions{

  formatNumberToAmount(n:any){
      return (n!='$ ')?'$ '+n.toString().replace(/\D/g,'').replace(/\B(?=(\d{3})+(?!\d))/g, "."):null;
  }

  formatOnlyNumber(n:any){
      return n.toString().replace(/\D/g,'')
  }

  formatOnlyNumberAndCC(n:any){
      return (n!='')?n.toString().replace(/\D/g,'')+' c.c.':null;
  }

  formatToUppercase(n:any){
      return n.toString().toUpperCase();
  }

  formatRut(n:string){
      return (n!=undefined)?n.replace(/(?!-)[^0123456789kK]/g,"").replace(/-/g,'').replace(/([0-9])([0-9kK]{1})$/,'$1-$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g,".").toUpperCase():null;
  }

  validateRut(rut:string) {
      let str = rut.toString().replace(/[.]/g,"");
      if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( str ))
        return false;
      var tmp 	= str.split('-');
      var digv	= tmp[1];
      var rut 	= tmp[0];
      if ( digv == 'K' ) digv = 'k' ;
      let dv = function(T){
        var M=0,S=1;
        for(;T;T=Math.floor(T/10))
          S=(S+T%10*(9-M++%6))%11;
        return S?S-1:'k';
      }
      return (dv(rut) == digv );
  }

  DateToStringFormat(date:Date,time?:boolean):string{
    let result = date.getFullYear()+"-"+("0"+(date.getMonth()+1)).slice(-2)+"-"+date.getDate();
    if(time){
      result+=" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()
    }
    return result;
  }
}
