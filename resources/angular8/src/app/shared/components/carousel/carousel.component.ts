import { Component, OnInit, Input } from '@angular/core';

export interface arrayphotos{
  url: string
  title?:string
  pub?:any
}

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {
  PublicationsPrimium:Array<any>;
  showNum:number=8;
  index = 0;
  @Input() avatars:arrayphotos[]=[];
  constructor() { }

  ngOnInit() {
  }

}
