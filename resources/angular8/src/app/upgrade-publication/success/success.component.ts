import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { DataBaseService, CreateUpgradePubRequest } from 'src/app/data-base.service';
import swal from 'sweetalert2';
import { functions } from 'src/app/shared/data/functions';

export interface getKhipuModelResponse {
  status:string,
  data?:KhipuData[]
}
export interface KhipuData{
  account_number : null
  amount: number
  bank_name: null
  "bill-id": string
  conciliation_date: null
  created_at: string
  currency: string
  custom: null
  discount: null
  id: number
  id_publication: number
  "manual-url": string
  "mobile-url": string
  notification_token: null
  notify_api_version: string
  out_of_date_conciliation: null
  payer_email: null
  payer_identifier: null
  payment_id: string
  "ready-for-terminal": number
  receiver_id: null
  status: string
  status_detail: null
  subject: string
  transaction_id: null
  updated_at: string
  url: string
}
@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit {
  data:KhipuData=null;
  constructor(private ActivatedRoute:ActivatedRoute, public DataBaseService:DataBaseService,private Router:Router,public functions:functions) { }

  ngOnInit() {
    this.data=null;
    this.ActivatedRoute.paramMap.subscribe((params:ParamMap) => {
      console.log(params);
      if(params.get("id")!=null){
        this.DataBaseService.GetKhipuById(params.get("id")).subscribe((resp:getKhipuModelResponse)=>{
          console.log("GetKhipuById::",resp);
          if(resp.status=="success" && resp.data!=null){
            if(resp.data[0]["ready-for-terminal"]==0){return}
            this.data = resp.data[0];
            let body:CreateUpgradePubRequest = {
              date_execute: this.functions.DateToStringFormat(new Date(),true),
              id_pay:resp.data[0].payment_id,
              id_pub:resp.data[0].id_publication.toString(),
            }
            this.DataBaseService.CreateUpgradePub(body).subscribe((reponse_CreateUpgradePub:{status:string})=>{
              console.log(reponse_CreateUpgradePub);
            })
          }
        },error=>{
          swal({text:"Id no encontrado..",type:"error",timer:2000}).then(()=>this.Router.navigateByUrl("/"));
        })
      }else{
        swal({text:"Faltan datos..",type:"error",timer:2000}).then(()=>this.Router.navigateByUrl("/"));
      }
    });
  }

}
