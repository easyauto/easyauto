import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpgradePublicationComponent } from './upgrade-publication.component';

describe('UpgradePublicationComponent', () => {
  let component: UpgradePublicationComponent;
  let fixture: ComponentFixture<UpgradePublicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpgradePublicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpgradePublicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
