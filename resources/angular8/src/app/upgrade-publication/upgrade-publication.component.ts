import { Component, OnInit, isDevMode } from '@angular/core';
import { DataBaseService, OptionsResponse } from '../data-base.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { MatRadioChange } from '@angular/material';

export interface inputGeneratePayKhipu{
	"transaction_id":string,//"000035"
  "amount":number,//4990
  "subject":string,//"Pago upgrade"
  "id_publication":number //76
  "item":string
}

export interface outputGeneratePayKhipu{
  "status": string, //"success"
  "data": {
    "id": string, //"uiiq68lt0yfw"
    "bill-id": string, //"gPxc1"
    "url": string, //"https://app.khipu.com/payment/show/uiiq68lt0yfw"
    "manual-url": string, //"https://khipu.com/payment/manual/uiiq68lt0yfw",
    "mobile-url": string, //"khipu:///pos/uiiq68lt0yfw",
    "ready-for-terminal":boolean //false
  }
}

@Component({
  selector: 'app-upgrade-publication',
  templateUrl: './upgrade-publication.component.html',
  styleUrls: ['./upgrade-publication.component.css', '../../styles2.css']
})
export class UpgradePublicationComponent implements OnInit {

  total:number=0;
  itemToPay:OptionsResponse=null;
  id_publication:number=null;
  subject:string=null;
  options:{checked1:boolean,checked2:boolean,checked3:boolean}={checked1:false,checked2:false,checked3:false};
  constructor(public DataBaseService:DataBaseService, private Router:Router) { }

  ngOnInit() {
    if(this.DataBaseService.cache!=null){
      this.id_publication=this.DataBaseService.cache.id;
      this.subject="Publicación Premiun";
      this.DataBaseService.Options.map(o=>o.checked=false)
    }else{
      this.Router.navigateByUrl("/");
    }
  }
  sumatory(e:MatRadioChange){
    this.total = e.value.amount;
    this.itemToPay = e.value;
  }
  GeneratePay(){
    if(this.total==0){swal({text:"Debe seleccionar una opción.",type:"warning"});return;}
    swal.showLoading();
    let generate = this.makeid(10);
    let inputKhipu:inputGeneratePayKhipu={
      amount:this.total,
      id_publication:this.id_publication,
      subject:this.itemToPay.title,
      transaction_id:this.id_publication+"-"+generate,
      item:JSON.stringify(this.itemToPay)
    };
    if(isDevMode())console.log(inputKhipu);
    this.DataBaseService.GeneratePayKhipu(inputKhipu).subscribe((response:outputGeneratePayKhipu)=>{
      swal.close();
      if(isDevMode())console.log(response);
      if(response.status == "success"){
        location.href = response.data.url;
      }
    },error=>{
      swal.close();
      if(isDevMode())console.log(error);
    })
  }
  makeid(length:number) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
}
