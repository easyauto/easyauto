import {Component, OnInit, Inject, Input} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataBaseService } from 'src/app/data-base.service';
import { functions } from 'src/app/shared/data/functions';
import swal from 'sweetalert2';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  pub:any
  formMessage:FormGroup;
  formMessageSend:boolean=false;
  @Input() dataInput = null;
  constructor(
    public DataBaseService:DataBaseService,
    private FB:FormBuilder,
    public functions:functions,
    public dialogRef: MatDialogRef<DetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(){
    if(this.data == null) this.pub = this.data.publication;
    if(this.dataInput == null) this.pub = this.dataInput;
    this.formMessage = this.FB.group({
      'publication_id':[this.pub.id,[Validators.required]],
      'email':[null,[Validators.required, Validators.email]],
      'phone':[null,[Validators.required, Validators.minLength(8)]],
      'name':[null,[Validators.required]],
      'message':[null,[Validators.required]],
      'copy':false
    })
  }

  sendMessage(){
    this.formMessageSend=true;setTimeout(() => this.formMessageSend=false, 3000);
    if(this.formMessage.valid){
      swal.showLoading();
      this.DataBaseService.SendMessageToPub(this.formMessage.value).subscribe((response:any)=>{
        swal.close();
        if(response.status == 'success'){
          swal({type:'success',text:'Tu mensaje fue enviado con exito!'})
          this.formMessage.reset();
        }else{
          if(response.message.email){
            swal({type:'error',text:'Tu mensaje NO fue enviado, por favor verifica tu email.'});
          }else{
            swal({type:'error',text:'Tu mensaje NO fue enviado, por favor verifica tus datos.'});
          }  
        }
      },error=>{swal.close();console.warn(error)})
    }
  }
}