import { Component, OnInit, Input } from '@angular/core';
import { DataBaseService } from "../data-base.service"
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, PageEvent } from '@angular/material';
import { DetailsComponent } from './details/details.component';
import { Router } from "@angular/router";
import swal from 'sweetalert2';

export interface region{
  id:number,
  name:string,
  number:string
}
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css','/../../styles2.css']
})
export class SearchComponent implements OnInit {
  PublicationsSearch:Array<any>=[];
  PublicationsSearchCache:Array<any>=[];
  Regions:region[]=[];
  pageSize:number=20;
  public search:Object
  constructor(public DB: DataBaseService,public dialog: MatDialog, private router: Router) { }

  ngOnInit() {
    swal.showLoading();
    if(this.DB.Search==''){this.DB.Search='%'}
      this.DB.PublicationsSearch(this.DB.Search).subscribe((data:any)=>{
        swal.close();
        if(data.publications_search.length>0){
          this.PublicationsSearchCache = data.publications_search.map((i) => {
            var vaRR = data.models.filter(model=>model.id == i.id_modul)[0];
            i.modelsString = vaRR.description;
            i.brandsString = data.brands.filter(brand=>brand.id == vaRR.id_brand)[0].description;
            return i;
          });

          console.log("data",this.PublicationsSearchCache);
          this.Regions = data.regions;
          this.changePage({pageIndex:0,pageSize:this.pageSize,length:this.PublicationsSearchCache.length});
        }
      },error=>swal("","Verifica tu conexion a internet!"))
  }
  OpenViewGallery(publication){
    this.router.navigate(['view_publication'],{queryParams:{publication:publication.id}})
    /*const dialogRef = this.dialog.open(DetailsComponent, {
      width: '80%',
      maxHeight:'80%',
      data: {publication}
    });*/
  }
  SearchRegions(id:number){
    return this.Regions.filter((r:region)=>r.id==id)[0].name;
  }
  changePage(e:PageEvent){
    this.PublicationsSearch = [];
    for (let i = (e.pageIndex*e.pageSize); i < (e.pageIndex*e.pageSize)+e.pageSize; i++) {
      const pub = this.PublicationsSearchCache[i];
      this.PublicationsSearch.push(pub);
    }
  }
}
