// MODULES
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { DataBaseService } from "./data-base.service";
import { NgSelectModule } from '@ng-select/ng-select';
import { MatDialogModule } from '@angular/material/dialog';
    // font-awesome
import { AngularFontAwesomeModule } from 'angular-font-awesome';
//COMPONENTS
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './menu/register/register.component';
import { LoginComponent } from './menu/login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProfileComponent } from './profile/profile.component';
import { SellEasyComponent } from './sell-easy/sell-easy.component';
import { ShowcaseComponent } from './home/showcase/showcase.component';
import { SearchComponent } from './search/search.component';
import { ShowcaseCarsEaturedComponent } from './home/showcase-cars-eatured/showcase-cars-eatured.component';
import { DetailsComponent } from './search/details/details.component';
import { CreditComponent } from './credit/credit.component';
import { InsuranceComponent } from './insurance/insurance.component';
import { AdministratorComponent } from './administrator/administrator.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { FaqComponent } from './faq/faq.component';
import { FooterComponent } from './footer/footer.component';
import { NgxHmCarouselModule } from 'ngx-hm-carousel';
//VARIABLES
import { AppValues } from './shared/data/app-values';
//PIPES
import { DateplusPipe } from './shared/pipes/dateplus.pipe';
import { CurrencyplusPipe } from './shared/pipes/currencyplus.pipe';
import { MileageplusPipe } from './shared/pipes/mileageplus.pipe';
import { TransmissionplusPipe } from './shared/pipes/transmissionplus.pipe';
import { GasplusPipe } from './shared/pipes/gasplus.pipe';
import { TractionplusPipe } from './shared/pipes/tractionplus.pipe';
import { AirbagplusPipe } from './shared/pipes/airbagplus.pipe';
import { DoorsplusPipe } from './shared/pipes/doorsplus.pipe';
import { RoofplusPipe } from './shared/pipes/roofplus.pipe';
import { NumOwnersplusPipe } from './shared/pipes/num-ownersplus.pipe';
import { BodyworkplusPipe } from './shared/pipes/bodyworkplus.pipe';
import { DirectionplusPipe } from './shared/pipes/directionplus.pipe';
import { TransmissionPlusPipe } from './shared/pipes/transmission-plus.pipe';
import { MatCheckboxModule, MatProgressBarModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatDatepickerModule, MAT_DATE_LOCALE, MatDatepicker, DateAdapter, NativeDateAdapter, MatNativeDateModule, MatButtonModule, MatRadioModule, MatPaginatorModule, MatPaginatorIntl, MatTabsModule } from '@angular/material';
import { functions } from './shared/data/functions';
import { CarouselComponent } from './shared/components/carousel/carousel.component';
import { PublicationsComponent } from './publications/publications.component';
import { MessagesComponent } from './messages/messages.component';
import { ViewComponent } from './publications/view/view.component';
import { PromotionsComponent } from './promotions/promotions.component';
import { MatStepperModule } from '@angular/material/stepper';
import { LandingCreditComponent } from './landing-credit/landing-credit.component';
import { ShortCreditComponent } from './short-credit/short-credit.component';
import { MatPaginatorIntlCro } from './shared/custom';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';

import { PdfViewerModule } from 'ng2-pdf-viewer';
import { UpgradePublicationComponent } from './upgrade-publication/upgrade-publication.component';
import { SuccessComponent } from './upgrade-publication/success/success.component';
import { ErrorComponent } from './upgrade-publication/error/error.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    PageNotFoundComponent,
    ProfileComponent,
    SellEasyComponent,
    ShowcaseComponent,
    SearchComponent,
    ShowcaseCarsEaturedComponent,
    DetailsComponent,
    CreditComponent,
    InsuranceComponent,
    DateplusPipe,
    CurrencyplusPipe,
    MileageplusPipe,
    TransmissionplusPipe,
    GasplusPipe,
    TractionplusPipe,
    AirbagplusPipe,
    DoorsplusPipe,
    RoofplusPipe,
    NumOwnersplusPipe,
    BodyworkplusPipe,
    DirectionplusPipe,
    TransmissionPlusPipe,
    AdministratorComponent,
    AboutUsComponent,
    FaqComponent,
    FooterComponent,
    CarouselComponent,
    PublicationsComponent,
    MessagesComponent,
    DetailsComponent,
    ViewComponent,
    PromotionsComponent,
    LandingCreditComponent,
    ShortCreditComponent,
    TermsAndConditionsComponent,
    UpgradePublicationComponent,
    SuccessComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    NgSelectModule,
    MatDialogModule,
    NgxHmCarouselModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatStepperModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatRadioModule,
    MatPaginatorModule,
    PdfViewerModule,
    MatTabsModule,
    // font-awesome
    AngularFontAwesomeModule,
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [
    DataBaseService,
    AppValues,
    functions,
    MatDatepicker,
    MatNativeDateModule,
    {provide: MAT_DATE_LOCALE, useValue: 'es-CL' },
    {provide: MatPaginatorIntl, useClass: MatPaginatorIntlCro }
  ],
  bootstrap: [AppComponent],
  entryComponents:[DetailsComponent,LoginComponent,RegisterComponent]
})
export class AppModule { }
