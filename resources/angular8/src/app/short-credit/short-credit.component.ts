import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import swal from 'sweetalert2';
import { DataBaseService } from '../data-base.service';
import { Router } from '@angular/router';
import { functions } from '../shared/data/functions';
import { Title, Meta } from '@angular/platform-browser';

export interface ArrayCombo{
  id?:string;
  name:string;
}

@Component({
  selector: 'app-short-credit',
  templateUrl: './short-credit.component.html',
  styleUrls: ['./short-credit.component.css']
})
export class ShortCreditComponent implements OnInit {
  HomeTypeArray:Array<ArrayCombo>;
  firstFormGroup: FormGroup;
  files:{f1:string,f2:string,f3:string,f4:string}={f1:"Cedula Identidad",f2:"Certificado AFP últimos 24 meses con Rut Empleador",f3:"Comprobante de domicilio vigente",f4:"Últimas 6 liquidaciones"};
  filesArray:File[]=[];
  constructor(public DB: DataBaseService,private formBuilder: FormBuilder,private router: Router, public functions:functions,private titleService: Title,private metaService: Meta) {
    this.HomeTypeArray=[{name:"Propia sin Deuda"},{name:"Propia con Deuda"},{name:"Arrendada"},{name:"De Familiares"},{name:"Fiscal o Empresa"}];
  }

  ngOnInit() {
    this.titleService.setTitle("Nuevo Crédito Automotriz | Easy Auto");
    this.metaService.updateTag({ name: 'description', content: 'Aprobación en 45. Llévate el auto que quieras en 24 horas. Con la cuota más baja' });
    this.firstFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      rut: ['', Validators.required],
      email: ['', Validators.required,Validators.email],
      phone: ['', Validators.required],
      account_type: ['', Validators.required],
    });
  }

  SendDataCredit(){

  }

  openButtonFile(id:string){
    document.getElementById(id).click();
  }

  Upload_Image(evt:any,id:string,i:number){
    let files:FileList = evt.target.files; // FileList object
    this.files["f"+i]=files[0].name;
    this.filesArray[i-1]=files[0];
  }

  sendData(){
    if(this.firstFormGroup.invalid){swal({text:"Debes completar el formulario!",type:"info"});return;}
    swal.showLoading();
    let post = new FormData();
    for (let i = 0; i < this.filesArray.length; i++) {
      const file = this.filesArray[i];
      post.append("file"+(i+1),file,file.name);
    }
    let keys_firstFormGroup = Object.keys(this.firstFormGroup.value);
    keys_firstFormGroup.forEach((key:string)=>{
      post.append(key,this.firstFormGroup.value[key]);
    });
    this.DB.SaveDataCreditShort(post).subscribe((response:any)=>{
      console.log("response_SaveDataCredit",response);
      if(response.status == "success"){
        swal({title:"Bienvenido a EasyAuto!",html:"Gracias por contactarnos, hemos recibido toda la información correctamente, un Broker se comunicará telefónicamente para coordinar una visita en caso de aprobación de tu Financiamiento.<br><br>Easy Auto La forma más fácil de comprar y vender tu auto.",type:"success",customClass:"swalCustom"}).then(()=>{
          this.router.navigateByUrl("/");
        });
      }else{
        swal({text:"En estos momentos no podemos procesar su solicitud!",type:"error"});
      }
    },error=>{swal({text:"Error al conectar con servidor",type:"error"});console.log(error);})
  }
}
