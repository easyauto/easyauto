import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataBaseService } from 'src/app/data-base.service';
import { functions } from 'src/app/shared/data/functions';
import swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  pub:any;
  formMessage:FormGroup;
  formMessageSend:boolean=false;
  @Input() dataInput = null;

  constructor(public DataBaseService:DataBaseService, private FB:FormBuilder, public functions:functions,private router: Router,public route:ActivatedRoute,private titleService: Title) {
      this.pub={photos:[]};
      swal.showLoading();
      this.route.queryParams.subscribe((params:any)=>{
        if(params.publication){
          this.DataBaseService.PublicationsGetById(params.publication).subscribe((response:any)=>{
            swal.close();
            if(response.publication){
              if(response.publication.id_status != 1){
                swal({type:'error',text:'Publicación ha finalizado...'}).then(()=>{
                  this.router.navigateByUrl("/");
                });
                return false;
              }
              this.AddVisit(params.publication);
              this.pub = response.publication;
              var vaRR = response.models.filter(model=>model.id == this.pub.id_modul)[0];
              this.pub.modelsString = vaRR.description;
              this.pub.brandsString = response.brands.filter(brand=>brand.id == vaRR.id_brand)[0].description;
              this.titleService.setTitle(this.pub.brandsString+" "+this.pub.modelsString+" "+this.pub.year+" | EasyAuto");
            }else{
              swal({type:'error',text:'Publicación no encontrada!'}).then(()=>{
                this.router.navigateByUrl("/");
              });
            }
          })
          this.formMessage = this.FB.group({
            'publication_id':[params.publication,[Validators.required]],
            'email':[null,[Validators.required, Validators.email]],
            'phone':[null,[Validators.required, Validators.minLength(8)]],
            'name':[null,[Validators.required]],
            'message':[null,[Validators.required]],
            'copy':false
          })
        }else{
            this.router.navigateByUrl("/");
        }
      })
  }

  ngOnInit(){

  }

  sendMessage(){
    this.formMessageSend=true;setTimeout(() => this.formMessageSend=false, 3000);
    if(this.formMessage.valid){
      swal.showLoading();
      this.DataBaseService.SendMessageToPub(this.formMessage.value).subscribe((response:any)=>{
        swal.close();
        if(response.status == 'success'){
          swal({type:'success',text:'Tu mensaje fue enviado con exito!'})
          this.formMessage.reset();
        }else{
          if(response.message.email){
            swal({type:'error',text:'Tu mensaje NO fue enviado, por favor verifica tu email.'});
          }else{
            swal({type:'error',text:'Tu mensaje NO fue enviado, por favor verifica tus datos.'});
          }
        }
      },error=>{swal.close();console.warn(error)})
    }else{
      swal({type:'warning',text:'Completa los campos'});
    }
  }

  AddVisit(id:string){
    let body = {
      url:this.route.toString(),
      id_publication:id
    }
    this.DataBaseService.AddVisits(body).subscribe((response:any)=>{
      console.log("add visit");
    })
  }
  verificType(pub:any,i:number):boolean{
    if(pub.id_type_publications==1){
      return(i<=9)?true:false;
    }else{
      return true;
    }
    return false;
  }

  //

  offerAmmount(e){
    let offerBtn = document.getElementById('offerBtn');

    if(e.target.value == 0)
    {
      offerBtn.classList.add('disabled'); 
    }
    else
    {
      let urlText = `https://api.whatsapp.com/send?phone=+56${this.pub.phone_pub}}&text=Hola%2C+vi+tu+publicacion+en+el+portal+de+easyauto+por+el+vehiculo+${this.pub.brandsString} ${this.pub.modelsString}%2C+quiero+ofertar+por+el+monto+de+ $ ${encodeURI(e.target.value)}`;
      offerBtn.setAttribute('href', urlText);
      offerBtn.classList.remove('disabled'); 
    }
  }
}
