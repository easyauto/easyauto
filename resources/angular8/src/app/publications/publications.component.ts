import { Component, OnInit } from '@angular/core';
import { DataBaseService } from '../data-base.service';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-publications',
  templateUrl: './publications.component.html',
  styleUrls: ['./publications.component.css']
})
export class PublicationsComponent implements OnInit {
  ArrayPublications:Array<any>
  ComboType:Array<any>
  constructor(public DB: DataBaseService,private router: Router) { }

  ngOnInit() {
    swal.showLoading();
    let user = JSON.parse(localStorage.getItem("user"));
    this.DB.PublicationsGet2(user.id).subscribe((data:any)=>{
      swal.close()
      this.ArrayPublications = data.publications.map((i) => {
        var vaRR = data.models.filter(model=>model.id == i.id_modul)[0];
        i.modelsString = vaRR.description;
        i.brandsString = data.brands.filter(brand=>brand.id == vaRR.id_brand)[0].description;
        return i;
      })
      this.ComboType = data.type_publications
    },error=>{swal.close();console.log(error);})
  }
  EditPublication(publication:any){
    this.router.navigate(['sell_easy'],{queryParams:{publication:publication.id}})
  }
  EliminarPublication(publication:any){
    swal({
      title: 'Estas seguro?',
      text: "Vas a eliminar tu publicacion!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar!'
    }).then((result) => {
      if (result.value) {
        this.DB.EliminarPublicacion(publication.id).subscribe((data:any)=>{
          swal.close();
          if(data.status=='success'){
            swal({type:'success'})
            this.ngOnInit()
          }else{
            swal({toast:true,type:'error'})
          }
        },error=>{swal.close();console.log(error);})
      }
    })

  }
  upgradePub(publication){
    this.router.navigate(['view_publication'],{queryParams:{publication:publication.id}})
    this.DB.cache = publication;
  }
  Upgrade(publication){
    this.DB.cache = publication;
    this.router.navigateByUrl("upgrade-publication");
  }
  showMessages(pub){
    this.DB.cache = null;
    this.DB.cache = pub;
    this.router.navigate(['messages']);
  }
}
