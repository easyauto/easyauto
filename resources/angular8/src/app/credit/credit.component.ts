import { DataBaseService } from "../data-base.service"
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";
import swal from "sweetalert2";
import { functions } from "../shared/data/functions";
import { Title, Meta } from "@angular/platform-browser";

export interface ArrayCombo{
  id?:string;
  name:string;
}
@Component({
  selector: 'app-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.css']
})
export class CreditComponent implements OnInit {
  public FormCredit: FormGroup;
  HomeTypeArray:Array<ArrayCombo>;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourFormGroup: FormGroup;
  files:{f1:string,f2:string,f3:string,f4:string}={f1:"Cedula Identidad",f2:"Certificado AFP últimos 24 meses con Rut Empleador",f3:"Comprobante de domicilio vigente",f4:"Últimas 6 liquidaciones"};
  filesArray:File[]=[];
  constructor(public DB: DataBaseService,private formBuilder: FormBuilder,private router: Router, public functions:functions,private titleService: Title,private metaService: Meta) {
    this.HomeTypeArray=[{name:"Propia sin Deuda"},{name:"Propia con Deuda"},{name:"Arrendada"},{name:"De Familiares"},{name:"Fiscal o Empresa"}];

    this.FormCredit = this.formBuilder.group({
      name:[null,[Validators.required]],
      lastname: [null, [Validators.required]],
      address:[null, [Validators.required]],
      departament:[null, [Validators.required]],
      street:[null, [Validators.required]],
      commune:[null, [Validators.required]],
      region:[null, [Validators.required]],
      rut:[null, [Validators.required]],
      email:[null, [Validators.email,Validators.required]],
      phone:[null, [Validators.required]],
      civil_status:[null, [Validators.required]],
      birtdate:[null, [Validators.required]],
      profession:[null, [Validators.required]],
      home_type:[null, [Validators.required]],
      rent:[null, [Validators.required]],
      employer_name:[null, [Validators.required]],
      employer_address:[null, [Validators.required]],
      employer_rut:[null, [Validators.required]],
      employer_phone:[null, [Validators.required]],
      contract_type:[null, [Validators.required]],
      employer_position:[null, [Validators.required]],
      contract_rent:[null, [Validators.required]],
      date_hire:[null, [Validators.required]],
      vehicle:[null, [Validators.required]],
      price_vehicle:[null, [Validators.required]],
      properties:[null, [Validators.required]],
      price_properties:[null, [Validators.required]],
      financing_vehicle:[null, [Validators.required]],
      price_financing_vehicle:[null, [Validators.required]],
      starting_amount:[null, [Validators.required]],
      number_parts:[null, [Validators.required]],
      origin_starting:[null, [Validators.required]],
      details:[null, [Validators.required]],
    });
  }

  ngOnInit() {
    this.titleService.setTitle("Nuevo Crédito Automotriz | Easy Auto");
    this.metaService.updateTag({ name: 'description', content: 'Aprobación en 45. Llévate el auto que quieras en 24 horas. Con la cuota más baja' });
    this.firstFormGroup = this.formBuilder.group({
      name: ['', Validators.required],
      rut: ['', Validators.required],
      birtdate: ['', Validators.required],
      address: ['', Validators.required],
      commune: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
      account_type: ['', Validators.required],
    });
    this.secondFormGroup  = this.formBuilder.group({
      car: ['', Validators.required],
      amount_car: ['', Validators.required],
      amount_init: ['', Validators.required],
      monthly_payment: ['', Validators.required],
    });
    this.thirdFormGroup  = this.formBuilder.group({
      name_employer: ['', Validators.required],
      rut_employer: ['', Validators.required],
      year_employer: ['', Validators.required],
      position_employer: ['', Validators.required],
      salary_employer: ['', Validators.required],
      salary_type_employer: ['', Validators.required],
    });
    this.fourFormGroup  = this.formBuilder.group({
      file1: [null, Validators.required],
      file2: [null, Validators.required],
      file3: [null, Validators.required],
      file4: [null, Validators.required],
    });
  }

  SendDataCredit(){

  }

  openButtonFile(id:string){
    document.getElementById(id).click();
  }

  Upload_Image(evt:any,id:string,i:number){
    let files:FileList = evt.target.files; // FileList object
    this.files["f"+i]=files[0].name;
    this.filesArray[i-1]=files[0];
  }

  sendData(){
    if(this.filesArray.length != 4){swal({text:"Debes incluir todos los requsitos adjuntos!",type:"info"});return;}
    swal.showLoading();
    let post = new FormData();
    for (let i = 0; i < this.filesArray.length; i++) {
      const file = this.filesArray[i];
      post.append("file"+(i+1),file,file.name);
    }
    let keys_firstFormGroup = Object.keys(this.firstFormGroup.value);
    keys_firstFormGroup.forEach((key:string)=>{
      post.append(key,this.firstFormGroup.value[key]);
    });
    let keys_secondFormGroup = Object.keys(this.secondFormGroup.value);
    keys_secondFormGroup.forEach((key:string)=>{
      post.append(key,this.secondFormGroup.value[key]);
    });
    let keys_thirdFormGroup = Object.keys(this.thirdFormGroup.value);
    keys_thirdFormGroup.forEach((key:string)=>{
      post.append(key,this.thirdFormGroup.value[key]);
    });

    this.DB.SaveDataCredit(post).subscribe((response:any)=>{
      console.log("response_SaveDataCredit",response);
      if(response.status == "success"){
        swal({title:"Bienvenido a EasyAuto!",html:"Gracias por contactarnos, hemos recibido toda la información correctamente, un Broker se comunicará telefónicamente para coordinar una visita en caso de aprobación de tu Financiamiento.<br><br>Easy Auto La forma más fácil de comprar y vender tu auto.",type:"success",customClass:"swalCustom"}).then(()=>{
          this.router.navigateByUrl("/");
        });
      }else{
        swal({text:"En estos momentos no podemos procesar su solicitud!",type:"error"});
      }
    },error=>{swal({text:"Error al conectar con servidor",type:"error"});console.log(error);})
  }

  setNewQuestion(){
    this.files.f4 = (this.thirdFormGroup.value.salary_type_employer=="FIJA")?"Últimas 3 liquidaciones":"Últimas 6 liquidaciones";
  }
}
