import { Component, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { DataBaseService } from "../data-base.service"
import { SearchComponent } from '../search/search.component';
import { AppValues } from './../shared/data/app-values';
import { arrayphotos } from '../shared/components/carousel/carousel.component';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  TextMenu:Object
  public FormSearch: FormGroup
  Regions:Array<any>
  PublicationsSearch:Array<any>
  public repeat_arrows:string='';
  public repeat_arrows_top:string='';
  public repeat_arrows_bot:string='';
  index = 0;
  data = ['1234567891234'];
  showNum=8;
  ListAlianzas:arrayphotos[]=AppValues.convenios;

  constructor(public AppValue:AppValues,public DB: DataBaseService, private router: Router,private route:ActivatedRoute,private formBuilder: FormBuilder,private titleService: Title ) {
    this.TextMenu =
    {
      "es_CL":{
        title:"Busca tu siguiente auto",
        brand:"Marca",
        model:"Modelo",
        region:"Region",
        price_min:"Precio Minimo",
        price_max:"Precio Maximo",
        year_start:"Año desde",
        year_end:"Año hasta",
        button_search:"Buscar",
        cars_featured:"Autos Destacados"
      },
      "en_US":{
        title:"Search your next vehicle",
        brand:"Brand",
        model:"Model",
        region:"Region",
        price_min:"Price low",
        price_max:"Price high ",
        year_start:"From the year",
        year_end:"To the year",
        button_search:"Search",
        cars_featured:"Featured Cars"
      }
    }
    this.PublicationsSearch=[];
    //this.data=this.AppValues.convenios;
  }

  ngOnInit() {
    this.titleService.setTitle("Publica tu auto Gratis y Vende Fácil | comprar y vender tu auto usado | Easy Auto");
    this.FormSearch = this.formBuilder.group({
      regions: ['', [Validators.required]],
    });
    this.DB.RegionsGet().subscribe((data:any)=>{
      this.Regions = data.regions
    })
    let interval = setInterval(() => {
      this.RepeatArrowsFn()
    }, 700);
  }
  RepeatArrowsFn(){
    if(this.repeat_arrows==''){
      this.repeat_arrows='repeat_arrows'
      this.repeat_arrows_top='repeat_arrows_top'
      this.repeat_arrows_bot='repeat_arrows_bot'
    }else{
      this.repeat_arrows=''
      this.repeat_arrows_top=''
      this.repeat_arrows_bot=''
    }
  }
  Search(){
    this.DB.Search = this.FormSearch.value.region
    this.router.navigate(['search']);
  }
}
