import { Component, OnInit, HostListener } from '@angular/core';
import { DataBaseService } from './../../data-base.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DetailsComponent } from './../../search/details/details.component';
import { Router } from '@angular/router';

export interface arrayphotos{
  url: string
  title:string
  pub:any
}

@Component({
  selector: 'app-showcase-cars-eatured',
  templateUrl: './showcase-cars-eatured.component.html',
  styleUrls: ['./showcase-cars-eatured.component.css']
})
export class ShowcaseCarsEaturedComponent implements OnInit {
  PublicationsPrimium:Array<any>;
  showNum:number=3;
  index = 0;
  avatars = '1234567891234'.split('').map((x, i) => {
    const num = i;
    return {
      url: null,
      title: `${num}`,
      pub:{modelsString:'',brandsString:''}
    };
  });
  
  
  constructor(private DataBaseService:DataBaseService,public dialog: MatDialog, private router: Router) { }
  ngOnInit() {
    if(window.screen.availWidth<=400){
      this.showNum=4;
    }else{
      this.showNum=6;
    }
    
    this.DataBaseService.GetPublicationsPrimium().subscribe((data:any)=>{
      this.avatars=[]
      this.PublicationsPrimium = data.publications_premium.map((i) => { 
        var vaRR = data.models.filter(model=>model.id == i.id_modul)[0];
        i.modelsString = vaRR.description; 
        i.brandsString = data.brands.filter(brand=>brand.id == vaRR.id_brand)[0].description; 
        this.avatars.push({url:i.photos[0].route,title:i.brandsString,pub:i})
        return i; 
      })
      
    },error=>{console.log("Error interno:::",error);
    })
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if(event.target.innerWidth<=400){
      this.showNum=4;
    }else{
      this.showNum=6;
    }
  }
  OpenViewGallery(publication){
    this.router.navigate(['view_publication'],{queryParams:{publication:publication.id}})
    /*const dialogRef = this.dialog.open(DetailsComponent, {
      width: '80%',
      maxHeight:'70%',
      data: {publication}
    });*/
  }
  click(i) {
    alert(`${i}`);
  }
}
