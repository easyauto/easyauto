import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-showcase',
  templateUrl: './showcase.component.html',
  styleUrls: ['./showcase.component.css']
})
export class ShowcaseComponent implements OnInit {
  image1:string = "url(/assets/images/home/home-1.png)";
  image2:string = "url(/assets/images/home/home-2.png)";
  movile:boolean=false;
  constructor() { }

  ngOnInit() {
    this.resizeWindow(window.innerWidth);
  }
  @HostListener('window:resize', ['$event'])
  onResize(event:any) {
    this.resizeWindow(event.target.innerWidth);
  }
  resizeWindow(w:number){
    if(w>425){
      this.image1 = "url(/assets/images/home/home-1.png)";
      this.image2 = "url(/assets/images/home/home-2.png)";
      this.movile = false;
    }else{
      this.image1 = "url(/assets/images/home/home-1-movile.png)";
      this.image2 = "url(/assets/images/home/home-2-movile.png)";
      this.movile = true;
    }
  }
}
