import { Injectable } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from './../environments/environment';
import swal from 'sweetalert2';
import { inputGeneratePayKhipu } from './upgrade-publication/upgrade-publication.component';

export class UserData {
  id:Number
  name:string
  email:string
  type_user:string
  created_at:Date
  updated_at:Date
}
export class UserLogInData {
  user:UserData
  token:string
}
export class CreateUpgradePubRequest {
  id_pay:string
  id_pub:string
  date_execute:string
}
export interface OptionsResponse{
  "amount": number
  "id": number
  "last_amount": number
  "sub_options": {
    "detail": string
    "id": number
    "options_id": number
    "subtitle": string
    "valid": boolean
  }[]
  "title":string,
  "checked":boolean
}
@Injectable({
  providedIn: 'root'
})
export class DataBaseService {
  Options:OptionsResponse[]=[];
  lang:string
  url:string// url del servidor
  UserLogIn:boolean
  UserLogInData:UserLogInData
  TextMenu:Object
  Search:any;
  cache:any;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization':'Bearer '+JSON.parse(localStorage.getItem("token"))
    })
  };
  httpOptionsFiles = {
    headers: new HttpHeaders({
      'Authorization':'Bearer '+JSON.parse(localStorage.getItem("token"))
    })
  };
  constructor(private http:HttpClient, private router: Router) {
    this.url=environment.host
    this.lang = "es_CL"
    this.TextMenu =
    {
      "es_CL":{
        msg_success:"Sesión Cerrada!",
      },
      "en_US":{
        msg_success:"Closed session!"
      }
    }
    this.UserLogIn = false
    this.UserLogInData = {user:null,token:null}
    this.loadConstants();
  }
  ErrorFn(e){
    if(e.status==401){
      this.logout()
    }
  }
  LogInPost(item: Object) {
    return this.http.post(this.url+'login', item);
  }
  RegisterPost(item: Object) {
    return this.http.post(this.url+'register', item);
  }
  logout(close_session:boolean=true){
    let token = JSON.parse(localStorage.getItem("token"))
    localStorage.removeItem("user")
    localStorage.removeItem("token")
    this.UserLogIn = false
    this.UserLogInData = {user:null,token:null}
    this.router.navigate(['/'])
    swal({text:this.TextMenu[this.lang].msg_success,type:'warning'})
    if(close_session==true){
      this.http.get(this.url+'logout', this.httpOptions).subscribe((data:any)=>{
        if(data.status=="success"){}//console.log("Close Session")
      })
    }
  }
  actualizateToken(){
    return {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization':'Bearer '+JSON.parse(localStorage.getItem("token"))
      })
    };
  }
  ValidateToken(){
    return this.http.get(this.url+'check', this.actualizateToken());
  }
  PublicationsGet(){
    return this.http.get(this.url+'publications_get', this.actualizateToken());
  }
  PublicationsPost(item: Object){
    return this.http.post(this.url+'publications',item, this.actualizateToken());
  }
  SaveImage(item: Object){
    return this.http.post(this.url+'SaveImageCache',item)
  }
  ChangeImagesNameCache(item: Object){
    return this.http.post(this.url+'ChangeImagesNameCache',item)
  }
  AddVisits(item: Object){
    return this.http.post(this.url+'visitsAdd',item)
  }
  PublicationsGetById(id:string){
    return this.http.get(this.url+'publications/'+id+"/edit", this.actualizateToken());
  }
  PublicationsPutById(id:string,item: Object){
    return this.http.put(this.url+'publications/'+id,item, this.actualizateToken());
  }
  EliminarPublicacion(id:string){
    return this.http.delete(this.url+'publications/'+id, this.actualizateToken());
  }
  PublicationsGet2(id:string){
    return this.http.get(this.url+'publications/'+id, this.actualizateToken());
  }
  GetPublicationsPrimium(){
    return this.http.get(this.url+'publications_premium', this.actualizateToken());
  }
  RegionsGet(){
    return this.http.get(this.url+'publications_search', this.actualizateToken());
  }
  PublicationsSearch(item:object){
    return this.http.post(this.url+'publications_search',item, this.actualizateToken());
  }
  UpdateUser(item:object){
    return this.http.put(this.url+'update_user/'+this.UserLogInData.user.id,item, this.actualizateToken());
  }
  UpdatePublicationType(item:object){
    return this.http.post(this.url+'UpdatePublicationType',item, this.actualizateToken());
  }
  GetPublicationAdmin(){
    return this.http.get(this.url+'GetPublicationAdmin', this.actualizateToken());
  }
  SendMessageToPub(body:any){
    return this.http.post(this.url+'send-msj-to-pub',body,this.actualizateToken());
  }
  SaveDataCredit(body:FormData){
    return this.http.post(this.url+'save-data-credit',body);
  }
  SaveDataCreditShort(body:FormData){
    return this.http.post(this.url+'save-data-credit-short',body);
  }
  togglePub(id:string){
    return this.http.get(this.url+'togglePub/'+id,this.actualizateToken());
  }
  GeneratePayKhipu(body:inputGeneratePayKhipu){
    return this.http.post(this.url+'khipu/create-payment',body,this.actualizateToken());
  }
  GetKhipuById(id:string){
    return this.http.get(this.url+'khipu/'+id,this.actualizateToken());
  }
  CreateUpgradePub(body:CreateUpgradePubRequest){
    return this.http.post(this.url+"create-upgrade-pub",body,this.actualizateToken());
  }
  loadConstants(){
    this.PublicationsGet().subscribe((resp:any)=>{
      console.log("loadConstants",resp);
      this.Options = resp.options;
    })
  }
  saveOptionsFn(){
    return this.http.post(this.url+"configuration-options/"+this.UserLogInData.user.id,this.Options,this.actualizateToken());
  }
}
