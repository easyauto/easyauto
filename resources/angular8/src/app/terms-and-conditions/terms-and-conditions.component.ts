import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.css']
})
export class TermsAndConditionsComponent implements OnInit {
  pdfSrc:string="/assets/files/Condiciones de uso Sistema Vende Fácil - Easy Auto.pdf";
  constructor() { }

  ngOnInit() {
  }

}
