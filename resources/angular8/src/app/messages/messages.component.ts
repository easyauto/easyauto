import { Component, OnInit, Input } from '@angular/core';
import { DataBaseService } from '../data-base.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  //@Input()data:any;
  data:any 
  constructor(public DB:DataBaseService, private router:Router) { }

  ngOnInit() {
    console.log(this.data);
    
    if( this.DB.cache != null){
      this.data = this.DB.cache;
    }else{
      this.router.navigate(['/']);
    }
  }


}
