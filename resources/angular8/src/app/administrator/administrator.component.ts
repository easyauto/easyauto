import { DataBaseService, OptionsResponse } from "../data-base.service"
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder,FormArray } from '@angular/forms';
import { Router } from "@angular/router";
import swal from "sweetalert2";
import { PageEvent } from "@angular/material";

@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.css']
})
export class AdministratorComponent implements OnInit {
  ArrayPublications:any[]=[]
  Options:any[]=[];
  Regions
  ComboType
  pub:any[]=[]
  FormPublications:FormGroup;
  pageSize:number=10;
  constructor(public DB: DataBaseService,private fb: FormBuilder,private router: Router) { }

  ngOnInit() {
    this.FormPublications = this.fb.group({
      pub:this.fb.array([]),
    })
    let token = JSON.parse(localStorage.getItem("token"))
    if(token!=null){
      this.DB.ValidateToken().subscribe((data:any)=>{
        if(data.status=="success"){
          this.DB.UserLogInData.user = JSON.parse(localStorage.getItem("user"));
          this.DB.UserLogInData.token = JSON.parse(localStorage.getItem("token"));
          this.DB.UserLogIn = true;
          if(this.DB.UserLogInData.user.type_user!='Administrator'){this.router.navigateByUrl("/home");}
          swal.showLoading();
          let publications = this.FormPublications.get(['pub']) as FormArray;
          setTimeout(() => {
            this.DB.GetPublicationAdmin().subscribe((data:any)=>{
              swal.close();
              this.ArrayPublications = data.publications.map((i) => {
                var vaRR = data.models.filter(model=>model.id == i.id_modul)[0];
                i.modelsString = vaRR.description;
                i.brandsString = data.brands.filter(brand=>brand.id == vaRR.id_brand)[0].description;
                return i;
              })
              this.Regions = data.regions
              this.ComboType = data.type_publications
              this.changePage({pageIndex:0,pageSize:this.pageSize,length:this.ArrayPublications.length});
            },error=>this.DB.ErrorFn(error))
          }, 200);
        }else{
          this.DB.logout()
        }
      },error=>this.DB.ErrorFn(error))
    }
  }
  UpdatePub(pub){
    if(pub.type!=null){
      this.DB.UpdatePublicationType({id:pub.id,type:pub.type}).subscribe((response:any)=>{
        if (response.status=='success'){
          swal({text:'Cambio Satisfactorio',type:'success'});
          this.ArrayPublications = response.publications.map((i) => {
            let r = i;
            var vaRR = response.models.filter(model=>model.id == i.id_modul)[0];
            r.modelsString = vaRR.description;
            r.brandsString = response.brands.filter(brand=>brand.id == vaRR.id_brand)[0].description;
            return r;
          })
          this.Regions = response.regions
          this.ComboType = response.type_publications
          //let publications = this.FormPublications.get(['pub']) as FormArray;
          //this.ArrayPublications.forEach(pub => publications.push(this.fb.group({id:pub.id,type:[pub.id_type_publications,[Validators.required]],photo:pub.photos[0].route,brandsString:pub.brandsString,modelsString:pub.modelsString,year:pub.year})));
          this.changePage({pageIndex:0,pageSize:this.pageSize,length:this.ArrayPublications.length});
        }else{
          swal({text:'Error al cambiar',type:'error'});
        }
      },error=>console.log("error",error))
    }
  }
  changePage(e:PageEvent){
    let publications = this.FormPublications.get(['pub']) as FormArray;
    while (publications.length !== 0) {
      publications.removeAt(0)
    }
    for (let i = (e.pageIndex*e.pageSize); i < (e.pageIndex*e.pageSize)+e.pageSize; i++) {
      const pub = this.ArrayPublications[i];
      publications.push(this.fb.group({
        id:pub.id,
        id_status:pub.id_status,
        type:[pub.id_type_publications,[Validators.required]],
        photo:(pub.photos.length>0)?pub.photos[0].route:"",
        brandsString:pub.brandsString,
        modelsString:pub.modelsString,
        year:pub.year
      }))
    }
  }
  togglePub(pub:any){
    swal.showLoading();
    let text = "La publicación no pudo ser "+(pub.id_status == 1)?"eliminada.":"publicada";
    this.DB.togglePub(pub.id).subscribe((response:{status:string})=>{
      if(response.status == "success"){
        this.DB.GetPublicationAdmin().subscribe((data:any)=>{
          swal.close();
          this.ArrayPublications = data.publications.map((i) => {
            var vaRR = data.models.filter(model=>model.id == i.id_modul)[0];
            i.modelsString = vaRR.description;
            i.brandsString = data.brands.filter(brand=>brand.id == vaRR.id_brand)[0].description;
            return i;
          })
          this.Regions = data.regions
          this.ComboType = data.type_publications
          this.changePage({pageIndex:0,pageSize:this.pageSize,length:this.ArrayPublications.length});
        },error=>this.DB.ErrorFn(error))
      }else{
        swal({text:text,type:"error"});
      }
    },error =>{
      swal({text:text,type:"error"});
      console.log(error);
    })
  }
  saveOptions(){
    swal.showLoading();
    this.DB.saveOptionsFn().subscribe((resp:{details:{options:OptionsResponse[]}})=>{
      swal({text:"Cambios guardados exitosamente"});
      this.DB.Options=resp.details.options;
    },error=>{
      swal.close();
      console.log(error);
    })
  }
  sumatory(){ }
}
