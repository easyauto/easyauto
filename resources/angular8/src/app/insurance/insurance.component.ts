import { DataBaseService } from "../data-base.service"
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";

@Component({
  selector: 'app-insurance',
  templateUrl: './insurance.component.html',
  styleUrls: ['./insurance.component.css']
})
export class InsuranceComponent implements OnInit {
  public FormInsurance: FormGroup

  constructor(public DB: DataBaseService,private formBuilder: FormBuilder,private router: Router) { 
    this.FormInsurance = this.formBuilder.group({
      name:[null,[Validators.required]],
      email: [null, [Validators.email,Validators.required]],
      rut:[null, [Validators.email,Validators.required]],
      phone:[null, [Validators.email,Validators.required]],
      direccion:[null, [Validators.email,Validators.required]],
      brand:[null, [Validators.email,Validators.required]],
      model:[null, [Validators.email,Validators.required]],
      year:[null, [Validators.email,Validators.required]],
      details:[null, [Validators.email,Validators.required]],
    });
  }

  ngOnInit() {
  }
  SendDataInsurance(){
    
  }
}
