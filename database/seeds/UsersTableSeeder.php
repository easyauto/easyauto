<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuarios = [
                [
                "name"=>"Eloy Prieto Jr",
                "email"=>"ing.eloyprieto@gmail.com",
                "password"=>Hash::make("spy123"),
                "type_user" => "Administrator"
            ],[
                "name"=>"Eloy Prieto 2",
                "email"=>"ing.eloyprieto2@gmail.com",
                "password"=>Hash::make("spy123"),
                "type_user" => "Client"
            ]
        ];
        foreach ($usuarios as $key => $usuario) {
            User::create($usuario);
        }
    }
}
