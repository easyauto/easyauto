<?php

use Illuminate\Database\Seeder;
use App\Regions;

class RegionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Regions = [
            [
                "name"=>"Metropolitana de Santiago",
                "number" => "XIII"
            ],[
                "name"=>"Tarapacá",
                "number" => "I"
            ],[
                "name"=>"Antofagasta",
                "number" => "II"
            ],[
                "name"=>"Atacama",
                "number" => "III"
            ],[
                "name"=>"Coquimbo",
                "number" => "IV"
            ],[
                "name"=>"Valparaíso",
                "number" => "V"
            ],[
                "name"=>"Libertador General Bernardo O’Higgins",
                "number" => "VI"
            ],[
                "name"=>"Maule",
                "number" => "VII"
            ],[
                "name"=>"Concepción",
                "number" => "VIII"
            ],[
                "name"=>"Araucanía",
                "number" => "IX"
            ],[
                "name"=>"Los Lagos",
                "number" => "X"
            ],[
                "name"=>"Aysén del General Carlos Ibañez del Campo",
                "number" => "XI"
            ],[
                "name"=>"Magallanes y de la Antártica Chilena",
                "number" => "XII"
            ],[
                "name"=>"Los Ríos",
                "number" => "XIV"
            ],[
                "name"=>"Arica y Parinacota",
                "number" => "XV"
            ],[
                "name"=>"Ñuble",
                "number" => "XVI"
            ]
        ];
       
        foreach ($Regions as $key => $Region) {
            Regions::create($Region);
        }
    }
}
