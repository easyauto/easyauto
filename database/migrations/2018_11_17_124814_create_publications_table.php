<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_status');
            $table->integer('id_type_publications');
            $table->integer('id_modul');
            $table->integer('id_user');
            $table->integer('id_regions');
            $table->integer('year');
            $table->integer('price');
            $table->integer('mileage');
            $table->timestamp('order');
            $table->timestamps();

            $table->string('patent')->nullable();
            $table->string('version')->nullable();
            $table->string('num_owners')->nullable();
            $table->integer('bodywork')->nullable();
            $table->integer('transmission')->nullable();
            $table->integer('displacement')->nullable();
            $table->integer('hp')->nullable();
            $table->integer('direction')->nullable();
            $table->integer('roof')->nullable();
            $table->integer('doors')->nullable();
            $table->integer('airbag')->nullable();
            $table->string('color')->nullable();
            $table->integer('traction')->nullable();
            $table->string('description')->nullable();
            $table->integer('gas')->nullable();

            $table->boolean('up_glasses')->nullable();
            $table->boolean('mirrors')->nullable();
            $table->boolean('close_door')->nullable();
            $table->boolean('air')->nullable();
            $table->boolean('weather')->nullable();
            $table->boolean('abs')->nullable();
            $table->boolean('radio')->nullable();
            $table->boolean('tires')->nullable();
            $table->boolean('debt')->nullable();
            $table->boolean('changeable')->nullable();
            $table->boolean('maintenance_agency')->nullable();

            $table->string('name_pub');
            $table->string('rut_pub');
            $table->string('phone_pub');
            $table->string('commune');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publications');
    }
}
