<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('type_user')->default("Client");

            $table->string('lastname1')->nullable();
            $table->string('lastname2')->nullable();
            $table->string('phone')->nullable();
            $table->string('rut')->nullable();
            $table->date('birthdate')->nullable();
            $table->integer('id_region')->nullable();
            $table->integer('id_city')->nullable();
            $table->integer('id_commune')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
