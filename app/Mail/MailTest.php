<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailTest extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    public $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data,$subject,$adj=null)
    {
        $this->data = $data;
        $this->subject = $subject;
        $this->adj = [];
        if(isset($adj)){
            $this->adj = $adj;
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->view('mails.test')->subject($this->subject);
        foreach($this->adj as $filePath => $fileParameters){
            $email->attach($filePath,$fileParameters);
        }
        return $email;
    }
}
