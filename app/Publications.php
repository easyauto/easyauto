<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publications extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_status',
        'id_type_publications',
        'id_modul',
        'id_user',
        'year',
        'price',
        'mileage',
        'id_regions',
        'name_pub',
        'rut_pub',
        'name_owner',
        'rut_owner',
        'phone_pub',
        'commune',
        "description",
        "gas",
        "abs",
        "air",
        "airbag",
        "bodywork",
        "close_door",
        "color",
        "debt",
        "direction",
        "displacement",
        "doors",
        "hp",
        "maintenance_agency",
        "mirrors",
        "num_owners",
        "patent",
        "radio",
        "roof",
        "tires",
        "traction",
        "transmission",
        "up_glasses",
        "version",
        "weather",
        "changeable",
        "order"
    ];
}
