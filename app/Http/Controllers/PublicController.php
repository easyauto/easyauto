<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Hash;
use App\User;
use App\Messages;
use DB;
use Mail;
use App\Mail\MailTest;

class PublicController extends Controller
{
    public function index(){
        $models = DB::table('models')->get();
        $brands = DB::table('brands')->get();
        $regions = DB::table('regions')->get();
        $type_publications = DB::table('type_publications')->get();

        $options = DB::table('options')->get();
        foreach ($options as $key => $option) {
            $options[$key]->sub_options = DB::table('sub_options')->where('options_id',$option->id)->get();
        }
        return response()->json([
            'brands'=>$brands,
            'options'=>$options,
            'models'=>$models,
            'regions'=>$regions,
            'type_publications'=>$type_publications
        ]);
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function SaveImage(Request $request)
    {
        $rules = [
            'file' => 'mimes:jpeg,png,bmp,tiff | max:20480 | required',
            'id' => 'required',
            'number' => 'required',
        ];
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => 'error','description'=>$validator]);
        }else{
            $id = $request->input('id');
            $number = $request->input('number');
            $path = public_path().'/assets/images/photos_vehicles/';
            $file = $request->file('file');
            $nameFileNew = 'cache-'.$id.'-'.mt_rand().'.'.explode(".",$file->getClientOriginalName())[1];
            $nameFile = 'cache-'.$id.'-'.$number.'.'.explode(".",$file->getClientOriginalName())[1];
            $path2 = $request->file->path();
            if(\File::exists($path.$nameFile)){
                rename($path.$nameFile,$path."delete".$nameFile);
                \File::delete($path."delete".$nameFile);
            }
            \File::copy($path2, $path.$nameFileNew);
            return response()->json([
                'status' => 'success',
                'name' => $nameFileNew,
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function DeleteImagen($imageName=null)
    {
        try {
            if($imageName!=null){
                $path = public_path().'/assets/images/photos_vehicles/';
                // Attempt to verify the credentials and create a token for the user
                if(\File::exists($path.$imageName)){
                    if(!\File::delete($path.$imageName))return response()->json(['status' => 'error','description' => 'Error to try delete']);
                }else{
                    return response()->json(['status' => 'error','description' => 'Image not found']);
                }
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'error','description' => 'Input invalid!']);
            }
        } catch (Exception $e) {
            // Something went wrong with JWT Auth.
            return response()->json([
                'status' => 'error'
            ], 409);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $credentials = $request->only('email', 'password','name');
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
            'name' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->messages()
            ]);
        }
        try {

            // Attempt to verify the credentials and create a token for the user
            if ($user = User::create([
                "name"=>$request->input("name"),
                "email"=>$request->input("email"),
                "password"=>Hash::make($request->input("password"))
            ])) {
                $data["title"]="Bienvenido a Easy Auto!";
                $data["content"]="La forma más facíl de comprar y vender tu auto!";
                //Mail::to($request->input("email"),$request->input("name"))->send(new MailTest($data));

                return response()->json([
                    'status' => 'success'
                ]);
            }
        } catch (Exception $e) {
            // Something went wrong with JWT Auth.
            return response()->json([
                'status' => 'error'
            ], 409);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function SendMsjToPub(Request $request){
        $post = $request->only('publication_id','email', 'phone','name','message');
        $rules = [
            'publication_id' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'name' => 'required',
            'message' => 'required'
        ];
        $validator = Validator::make($post, $rules);
        if($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->messages()
            ]);
        }else{
            try {

                // Attempt to verify the credentials and create a token for the user
                if ($insert = Messages::create([
                    "name"=>$request->input("name"),
                    "email"=>$request->input("email"),
                    "publication_id"=>$request->input("publication_id"),
                    "phone"=>$request->input("phone"),
                    "message"=>$request->input("message")
                ])) {
                    $pub = DB::table("publications")->where("id",$request->input("publication_id"))->get();
                    $model = DB::table("models")->where("id",$pub[0]->id_modul)->get()[0];
                    $brand = DB::table("brands")->where("id",$model->id_brand)->get()[0]->description;
                    $subtitle = " ".$brand." ".$model->description." ".$pub[0]->year." ";
                    $userSelected = User::where("id",$pub[0]->id_user)->get()[0];
                    $data["content"]=[];
                    $data["name"]=$userSelected["name"];
                    $data["pub"]=$post;
                    $data["title"]="Haz recibido un mensaje en tu publicación!";
                    $data["content"][]=$request->input("name")." te ha enviado un mensaje: ".$request->input("message")." desde su correo: ".$request->input("email").". Llámalo al número ".$request->input("phone");
                    Mail::to($userSelected["email"],$userSelected["name"])->send(new MailTest($data,"Tienes un nuevo mensaje por tu".$subtitle."desde EasyAuto"));
                    if($request->input("copy")==true){
                        $data["title"]="Copia del mensaje enviado al publicante!";
                        Mail::to($request->input("email"),$request->input("name"))->send(new MailTest($data,"Copia Tienes un nuevo mensaje por tu".$subtitle."desde EasyAuto"));
                    }

                    return response()->json([
                        'status' => 'success'
                    ]);
                }
            } catch (Exception $e) {
                // Something went wrong with JWT Auth.
                return response()->json([
                    'status' => 'error'
                ], 409);
            }
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function SaveDataCredit(Request $request){
        $rules = [
            'file1' => 'max:20480 | required',
            'file2' => 'max:20480 | required',
            'file3' => 'max:20480 | required',
            'file4' => 'max:20480 | required',

            'name' => 'required',
            'rut' => 'required',
            'birtdate' => 'required',
            'address' => 'required',
            'commune' => 'required',
            'email' => 'required',
            'phone' => 'required',

            'car' => 'required',
            'amount_car' => 'required',
            'amount_init' => 'required',
            'monthly_payment' => 'required',

            'rut_employer' => 'required',
            'name_employer' => 'required',
            'year_employer' => 'required',
            'position_employer' => 'required',
            'salary_employer' => 'required',
            'salary_type_employer' => 'required',
            'account_type' => 'required',
        ];
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => 'error','description'=>$validator->errors()]);
        }else{
            $post = $request->all();
            $data["content"]=[];
            $data["name"]=$request->input("name");
            $data["title"]="Formulario de credito !";
            foreach ($post as $key => $value) {
                if($value != null)$data["content"][]=$key.": ".$value;
            }
            $nameFile1 = $request->file('file1');
            $nameFile2 = $request->file('file2');
            $nameFile3 = $request->file('file3');
            $nameFile4 = $request->file('file4');

            $adj = [
                $nameFile1->path() => [
                    'as' => $nameFile1->getClientOriginalName(),
                ],
                $nameFile2->path() => [
                    'as' => $nameFile2->getClientOriginalName(),
                ],
                $nameFile3->path() => [
                    'as' => $nameFile3->getClientOriginalName(),
                ],
                $nameFile4->path() => [
                    'as' => $nameFile4->getClientOriginalName(),
                ],
            ];
            Mail::to("contacto@easyauto.cl","Administrador")->send(new MailTest($data,"Nuevo formulario de credito!",$adj));
            Mail::to("ing.eloyprieto@gmail.com","Administrador")->send(new MailTest($data,"Nuevo formulario de credito!",$adj));
            return response()->json([
                'status' => 'success',
                'request' => $nameFile4
            ]);
        }
    }
/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function SaveDataCredit2(Request $request){
        $rules = [
            'name' => 'required',
            'rut' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'account_type' => 'required',
        ];
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => 'error','description'=>$validator->errors()]);
        }else{
            $post = $request->all();
            $data["content"]=[];
            $data["name"]=$request->input("name");
            $data["title"]="Formulario de credito !";
            foreach ($post as $key => $value) {
                if($value != null)$data["content"][]=$key.": ".$value;
            }
            Mail::to("contacto@easyauto.cl","Administrador")->send(new MailTest($data,"Nuevo formulario de credito!"));
            Mail::to("ing.eloyprieto@gmail.com","Administrador")->send(new MailTest($data,"Nuevo formulario de credito!"));
            return response()->json([
                'status' => 'success',
                'request' => $post
            ]);
        }
    }
}
