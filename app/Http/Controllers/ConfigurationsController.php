<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class ConfigurationsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateOptions(Request $request, $id){
        $post = $request->all();
        try {
            $userSelected = User::where("id",$id)->get();
            if(count($userSelected)==0){ return $this->outputUpdateOptions(1,"User not found",406); }
            if($userSelected[0]->type_user != "Administrator"){ return $this->outputUpdateOptions(1,"User Unauthorized",406); }
            foreach ($post as $key => $option) {
                DB::table('options')->where('id',$option['id'])->update([
                    "title"=>$option['title'],
                    "last_amount"=>$option['last_amount'],
                    "amount"=>$option['amount'],
                ]);
                foreach ($option['sub_options'] as $key2 => $sub_option) {
                    DB::table('sub_options')->where('id',$sub_option['id'])->update([
                        "subtitle"=>$sub_option['subtitle'],
                    ]);
                }
            }
            $this->SaveLog($post,$id);
            $options_new = DB::table('options')->get();
            foreach ($options_new as $key => $option_new) {
                $options_new[$key]->sub_options = DB::table('sub_options')->where('options_id',$option_new->id)->get();
            }
            return $this->outputUpdateOptions(0,["options"=>$options_new]);
        } catch (\Exception $e) {
            return $this->outputUpdateOptions(99,$e->getMessage()."\n",406);
        }
    }
   
    private function outputUpdateOptions($code,$detail="",$httpCode=200){
        return response()->json(['status' => $code,'detail'=> $detail],$httpCode);
    }

    private function SaveLog($new,$idUser){
        try {
            $options = DB::table('options')->get();
            foreach ($options as $key => $option) {
                $options[$key]->sub_options = DB::table('sub_options')->where('options_id',$option->id)->get();
            }
            \DB::table("logs_config")->insert([
                "before"=>json_encode($options),
                "after"=>json_encode($new),
                "user_id"=>$idUser,
                "datetime"=>new \DateTime()
            ]);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

}
