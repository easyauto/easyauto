<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use App\Publications;
use Illuminate\Support\Facades\Storage;
use Mail;
use App\Mail\MailTest;

class PublicationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["name"]="name";
        $data["pub"]="";
        $data["title"]="!!! Felicitaciones !!!";
        $data["content"]=[];
        $data["content"][]="Haz publicado exitosamente.";
        $data["content"][]="Tu vehículo lo puedes ver en el siguiente link:";
        $data["content"][]="https://www.easyauto.cl/";
        $data["content"][]="¡Comparte tu aviso publicado con tus amigos en las redes sociales!";
        Mail::to("ing.eloyprieto@gmail.com","eloy prieto")->send(new MailTest($data,"Your Reminder2222!"));
        return response()->json([
            'status' => 'success',
        ]);
    }

    function optimizar_imagen($origen, $destino, $calidad) {
            $info = getimagesize($origen);
            if (strtolower($info['mime']) == 'image/jpeg'){
                if(imagecreatefromjpeg($origen))$imagen = imagecreatefromjpeg($origen);
            }else if (strtolower($info['mime']) == 'image/gif'){
                if(imagecreatefromgif($origen))$imagen = imagecreatefromgif($origen);
            }else if (strtolower($info['mime']) == 'image/png'){
                if(imagecreatefrompng($origen))$imagen = imagecreatefrompng($origen);
            }
            imagejpeg($imagen, $destino, $calidad);
            return true;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function changeNameImages(Request $request){
        $rules = [
            'imagesData' => 'required',
            'id_publication' => 'required',
            'main'=>'required',
            'method' => 'required'
        ];
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => 'error','description'=>$validator]);
        }else{
            $post = $request->all();
            $path = public_path().'/assets/images/photos_vehicles/';
            if($post['method']=="update"){
                DB::table('photos')->where('id_publication',$post['id_publication'])->delete();
            }
            foreach ($post['imagesData'] as $key => $imageName) {
                $newName = $post['id_publication']."-".mt_rand().".".explode(".",$imageName)[1];
                if($post['main']==$key){
                    $imageNameMain = '/assets/images/photos_vehicles/'.$newName;
                }
                //$this->optimizar_imagen($path.$imageName,$path.$newName, 20);
                rename($path.$imageName,$path.$newName);
                DB::table('photos')->insert([
                    'id_publication' => $post['id_publication'],
                    'route'=>'/assets/images/photos_vehicles/'.$newName
                ]);
            }
            if($imageNameMain){
                DB::table('photos')->where("route",$imageNameMain)->update([
                    "main"=>1
                ]);
            }
        }
        return response()->json([
            'status' => 'success',
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = $request->all();

        $insert = Publications::create([
            "id_regions"=>$post['regions'],
            "id_status"=>1,
            "name_pub"=>$post['name_pub'],
            "rut_pub"=>$post['rut_pub'],
            // "name_owner"=>$post['name_owner'],
            // "rut_owner"=>$post['rut_owner'],
            "phone_pub"=>$post['phone_pub'],
            "commune"=>$post['commune'],
            "id_type_publications"=>1,
            "id_modul"=>$post['model'],
            "id_user"=>$post['id_user'],
            "year"=>$post['year'],
            "price"=>preg_replace('/\D/', '', $post['price']),
            "mileage"=>$post['mileage'],
            "description"=>$post['description'],
            "gas"=>$post['gas'],
            // No requered
            "abs"=>$post['abs'],
            "air"=>$post['air'],
            "airbag"=>$post['airbag'],
            "bodywork"=>$post['bodywork'],
            "close_door"=>$post['close_door'],
            "color"=>$post['color'],
            "debt"=>$post['debt'],
            "direction"=>$post['direction'],
            "displacement"=>$post['displacement'],
            "doors"=>$post['doors'],
            "hp"=>$post['hp'],
            "maintenance_agency"=>$post['maintenance_agency'],
            "mirrors"=>$post['mirrors'],
            "num_owners"=>$post['num_owners'],
            "patent"=>$post['patent'],
            "radio"=>$post['radio'],
            "roof"=>$post['roof'],
            "tires"=>$post['tires'],
            "traction"=>$post['traction'],
            "transmission"=>$post['transmission'],
            "up_glasses"=>$post['up_glasses'],
            "version"=>$post['version'],
            "weather"=>$post['weather'],
            "changeable"=>$post['changeable'],
            "order"=>\Carbon\Carbon::now()
        ])->id;

        if($insert){
            $userSelected = User::where("id",$post['id_user'])->get()[0];
            $data["name"]=$userSelected["name"];
            $data["pub"]="";
            $data["title"]="!!! Felicitaciones !!!";
            $data["content"]=[];
            $data["content"][]="Haz publicado exitosamente.";
            $data["content"][]="Tu vehículo lo puedes ver en el siguiente link:";
            $data["content"][]="https://www.easyauto.cl/view_publication?publication=".$insert;
            $data["content"][]="¡Comparte tu aviso publicado con tus amigos en las redes sociales!";
            Mail::to($userSelected["email"],$userSelected["name"])->send(new MailTest($data,"Te damos la Bienvenida Easy Auto"));
            return response()->json([
                'status' => 'success',
                'id_publication' => $insert
            ]);
        }else{
            return response()->json([
                'status' => 'error',
            ],405);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $publications = DB::table('publications')->where('id_status',1)->where('id_user',$id)->orderBy('created_at','desc')->get();
        $models = DB::table('models')->get();
        $brands = DB::table('brands')->get();
        $regions = DB::table('regions')->get();
        $type_publications = DB::table('type_publications')->get();
        foreach ($publications as $key => $publication) {
            $publications[$key]->photos = DB::table('photos')->where('id_publication',$publication->id)->orderBy('main','desc')->get();
            $publications[$key]->messages = DB::table('messages')->where('publication_id',$publication->id)->get();
            try {
                $publications[$key]->visit = DB::table('counters')->where('id_publication',$publication->id)->get()[0]->visits;
            } catch (\Throwable $th) {
                $publications[$key]->visit = 0;
            }
        }
        return response()->json([
            'publications' => $publications,
            'brands'=>$brands,
            'models'=>$models,
            'regions' => $regions,
            'type_publications'=>$type_publications,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $publications = DB::table('publications')->where('id',$id)->get()[0];
        $models = DB::table('models')->get();
        $brands = DB::table('brands')->get();
        $regions = DB::table('regions')->get();
        $type_publications = DB::table('type_publications')->get();
        $publications->photos = DB::table('photos')->where('id_publication',$id)->orderBy('main','desc')->get();
        return response()->json([
            'publication' => $publications,
            'brands' => $brands,
            'models' => $models,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = $request->all();
        $update = Publications::where("id",$id)->update([
            "id_regions"=>$post['regions'],
            "id_status"=>1,
            "name_pub"=>$post['name_pub'],
            "rut_pub"=>$post['rut_pub'],
            // "name_owner"=>$post['name_owner'],
            // "rut_owner"=>$post['rut_owner'],
            "phone_pub"=>$post['phone_pub'],
            "commune"=>$post['commune'],
            //"id_type_publications"=>$post['type_publications'],
            "id_modul"=>$post['model'],
            "id_user"=>$post['id_user'],
            "year"=>$post['year'],
            "price"=>preg_replace('/\D/', '', $post['price']),
            "mileage"=>$post['mileage'],
            "description"=>$post['description'],
            "gas"=>$post['gas'],
            // No requered
            "abs"=>$post['abs'],
            "air"=>$post['air'],
            "airbag"=>$post['airbag'],
            "bodywork"=>$post['bodywork'],
            "close_door"=>$post['close_door'],
            "color"=>$post['color'],
            "debt"=>$post['debt'],
            "direction"=>$post['direction'],
            "displacement"=>$post['displacement'],
            "doors"=>$post['doors'],
            "hp"=>$post['hp'],
            "maintenance_agency"=>$post['maintenance_agency'],
            "mirrors"=>$post['mirrors'],
            "num_owners"=>$post['num_owners'],
            "patent"=>$post['patent'],
            "radio"=>$post['radio'],
            "roof"=>$post['roof'],
            "tires"=>$post['tires'],
            "traction"=>$post['traction'],
            "transmission"=>$post['transmission'],
            "up_glasses"=>$post['up_glasses'],
            "version"=>$post['version'],
            "weather"=>$post['weather'],
            "changeable"=>$post['changeable']
        ]);
        // DB::table('photos')->where('id_publication',$id)->delete();
        // foreach ($post['imagesData'] as $key => $image) {
        //     $data = explode(',', $image );
        //     $data2 = explode('/',$data[0]);
        //     $data3 = explode(';',$data2[1]);
        //     $path = 'assets/images/photos_vehicles/'.$id.'-'.$key.'-photo.'.$data3[0];
        //     DB::table('photos')->insert([
        //         'id_publication' => $id,
        //         'route'=>$path
        //     ]);
        //     file_put_contents($path, file_get_contents($image));
        //     //file_put_contents(base_path()."/resources/angular/src/".$path, file_get_contents($image));
        // }
        //DB::table('photos')->where('id_publication',$id)->delete();
        if($post){
            $userSelected = User::where("id",$post['id_user'])->get()[0];
            $data["name"]=$userSelected["name"];
            $data["pub"]=$post;
            $data["title"]="La modificación de tu publicación ya se encuenta online!";
            $data["content"]=[];
            $data["content"][]="Puedes verificarlo en:";
            $data["content"][]="http://www.easyautochile.cl/view_publication?publication=".$id;
            $data["content"][]="¡Comparte tu aviso publicado con tus amigos en las redes sociales!";
            //Mail::to($userSelected["email"],$userSelected["name"])->send(new MailTest($data));
            return response()->json([
                'status' => 'success',
                'post' => $post
            ]);
        }else{
            return response()->json([
                'status' => 'error',
            ],405);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateuser(Request $request, $id)
    {
        $post = $request->all();

        $update = User::where('id',$id)->update([
            'name'=>$post['name'],
            'birthdate'=>$post['birthdate'],
            'id_region'=>$post['regions'],
            'lastname1'=>$post['lastname1'],
            'lastname2'=>$post['lastname2'],
            'phone'=>$post['phone'],
            'rut'=>$post['rut'],
            'id_commune'=>$post['commune'],
            'id_city'=>$post['city'],
        ]);
        $user = User::where('id',$id)->get()[0];
        if($update){
            return response()->json([
                'status' => 'success',
                'user' => $user,
            ]);
        }else{
            return response()->json([
                'status' => $post,
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function Regions()
    {
        $regions = DB::table('regions')->get();
        return response()->json([
            'regions' => $regions,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Search(Request $request)
    {
        $post = $request->all();
        $models = DB::table('models')->get();
        $brands = DB::table('brands')->get();
        $regions = DB::table('regions')->get();
        if(!isset($post['regions'])){
            $publications_search = DB::table('publications')->where('id_status',1)->orderBy('order',"desc")->get();
        }else{
            $publications_search = DB::table('publications')->where('id_status',1)->where('id_regions',$post['regions'])->orderBy('order',"desc")->get();
        }
        foreach ($publications_search as $key => $publication) {
            $publications_search[$key]->photos = DB::table('photos')->where('id_publication',$publication->id)->orderBy('main','desc')->get();
            $publications_search[$key]->featureds = DB::table('featureds')->where('id_publication',$publication->id)->get();
        }
        return response()->json([
            'publications_search' => $publications_search,
            'brands'=>$brands,
            'models'=>$models,
            'regions'=>$regions,
        ]);
    }
    public function publications_premium(){
        $publications_search = DB::table('publications')->where('id_status',1)->where('id_type_publications',"3")->get();
        $models = DB::table('models')->get();
        $brands = DB::table('brands')->get();
        foreach ($publications_search as $key => $publication) {
            $publications_search[$key]->photos = DB::table('photos')->where('id_publication',$publication->id)->orderBy('main','desc')->get();
            $publications_search[$key]->contact = User::where('id',$publication->id_user)->select('phone','email')->get()[0];
        }
        return response()->json([
            'publications_premium' => $publications_search,
            'brands'=>$brands,
            'models'=>$models,
        ]);
    }

    public function visitsAdd(Request $request){
        $post = $request->all();
        $visits = DB::table('counters')->where("id_publication",$post["id_publication"])->get();
        if(count($visits)>0){
            $update =  DB::table('counters')->where("id_publication",$post["id_publication"])->update([
                'visits'=>$visits[0]->visits+1,
                "last"=> new \DateTime()
            ]);
        }else{
            $insert = DB::table('counters')->insert([
                "visits"=>1,
                "id_publication"=>$post['id_publication'],
                "last"=> new \DateTime()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function togglePublication($id)
    {
        $pubs = DB::table('publications')->where('id',$id)->get();
        if(count($pubs)>0){
            $update = DB::table('publications')->where('id',$id)->update([
                "id_status"=>($pubs[0]->id_status == 1)?3:1,
            ]);
        }
        return response()->json([
            'status' => ($update)?'success':'error'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(count(DB::table('publications')->where('id',$id)->get())>0){
            $update = DB::table('publications')->where('id',$id)->update([
                "id_status"=>2,
            ]);
        }
        return response()->json([
            'status' => ($update)?'success':'error'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function UpdatePublicationType(Request $request){
        $post = $request->all();
        if(count(DB::table('publications')->where('id',$post['id'])->get())>0){
            $update = DB::table('publications')->where('id',$post['id'])->update([
                "id_type_publications"=>$post['type'],
            ]);
        }
        $publications = DB::table('publications')->get();
        foreach ($publications as $key => $publication) {
            $publications[$key]->photos = DB::table('photos')->where('id_publication',$publication->id)->orderBy('main','desc')->get();
        }
        if($update){
            return response()->json([
                'status' => 'success',
                'publications' => $publications,
                'brands'=>DB::table('brands')->get(),
                'models'=>DB::table('models')->get(),
                'regions' => DB::table('regions')->get(),
                'type_publications'=>DB::table('type_publications')->get(),
            ]);
        }else{
            return response()->json([
                'status' => 'error',
                'publications' => DB::table('publications')->get()
            ]);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function GetPublicationAdmin(){
        $publications = DB::table('publications')->orderBy('created_at','desc')->get();
        $models = DB::table('models')->get();
        $brands = DB::table('brands')->get();
        $regions = DB::table('regions')->get();
        $type_publications = DB::table('type_publications')->get();
        foreach ($publications as $key => $publication) {
            $publications[$key]->photos = DB::table('photos')->where('id_publication',$publication->id)->orderBy('main','desc')->get();
        }
        return response()->json([
            'publications' => $publications,
            'brands'=>$brands,
            'models'=>$models,
            'regions' => $regions,
            'type_publications'=>$type_publications,
        ]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreateUpdatePub(Request $request){
        $rules = [
            'id_pub' => 'required',
            'id_pay' => 'required',
            'date_execute' => 'required',
        ];
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => 'error','description'=>$validator->errors()],430);
        }else{
            $post = $request->all();
            try {
                DB::table('updates')->insert([
                    "id_pub"=>$post['id_pub'],
                    "id_pay"=>$post['id_pay'],
                    "date_execute"=>$post['date_execute'],
                    "executed"=>0,
                ]);
                //code...
                return response()->json([
                    'status'=>"0",
                ]);
            } catch (\Throwable $th) {
                return response()->json([
                    'status'=>"450",
                    'description'=>"Error al guardar",
                    'detail'=>$th
                ],450);
            }
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function UpdatePubs(){
        $today = \Carbon\Carbon::now();
        $updates = DB::table('updates')->where("date_execute","<=",$today)->where("executed",0)->get();
        foreach ($updates as $key => $update) {
            Publications::where("id",$update->id_pub)->update([
                "order"=>$today
            ]);
            DB::table('updates')->where("id",$update->id)->update([
                "executed"=>1,
                "date_executed"=>$today
            ]);
        }
        if(count($updates)>0){
            return response()->json([
                'status'=>"0",
            ]);
        }else{
            return response()->json([
                'status'=>"10",
            ],450);
        }
    }
}
