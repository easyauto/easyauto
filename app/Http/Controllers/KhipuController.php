<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KhipuHistory;
use DB;
use App\User;
use App\Publications;
class KhipuController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function CreatePayment(Request $request){
        //  Crear un pago
        $rules = [
            'transaction_id' => 'required',
            'amount' => 'required',
            'id_publication' => 'required',
            'item' => 'required',
            'subject' => 'required'
        ];
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json(['status' => 'error','description'=>$validator->errors()]);
        }else{
            $post = $request->all();
            try {
                $getPublicationId = Publications::where("id",$post["id_publication"])->first()["id_user"];
                $getUser = User::where("id",$getPublicationId)->first();
                $data = array(
                    "transaction_id" => $post["transaction_id"],
                    "return_url" => "https://www.easyauto.cl/khipu-success/".$post["transaction_id"],
                    "cancel_url" => "https://www.easyauto.cl/khipu-cancel/".$post["transaction_id"],
                    "picture_url"=> "https://www.easyauto.cl/assets/images/logo.png",
                    "notify_url" => "https://www.easyauto.cl/api/khipu/notify",
                    "notify_api_version" => "1.3",
                    "subject" => $post["subject"], //Motivo de la compra
                    "amount" => $post["amount"], //Monto

                    "payer_name" => $getUser->name,
                    "payer_email" => $getUser->email,
                    "send_email" => true,
                    "send_reminders" => true,
                );
                $Khipu = \Khipu::loadService('CreatePaymentURL');
                $Khipu->setParameters($data);
                $response = json_decode($Khipu->createUrl(),true);

                KhipuHistory::create([
                    "transaction_id"=>$post["transaction_id"],
                    "subject"=>$post["subject"],
                    "currency"=>"CLP",
                    "amount"=>$post["amount"],
                    "id_publication"=>$post["id_publication"],
                    "notify_api_version"=>"1.3",
                    "bill-id"=> $response["bill-id"],
                    "manual-url"=>$response["manual-url"],
                    "mobile-url"=>$response["mobile-url"],
                    "ready-for-terminal"=>false,
                    "payment_id"=>$response["id"],
                    "url"=>$response["url"],
                    "item"=>$post["item"],
                ]);
                return response()->json([
                    'status' => 'success',
                    'data' => $response,
                    "payer_name" => $getUser->name,
                    "payer_email" => $getUser->email,
                ]);
            } catch (\Khipu\ApiException $e) {
                return response()->json([
                    'status' => 'error',
                    'detail' => $e->getResponseBody()
                ],500);
            }
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function VerificPaymentById($id){
        $get = KhipuHistory::where("transaction_id",$id)->get();
        if(count($get)>0){
            return response()->json([
                'status' => 'success',
                'data'  => $get
            ]);
        }else{
            return response()->json([
                'status' => 'error',
            ],430);
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function GetPayment(Request $request){
        //  Recibiendo confirmacion
        $post = $request->all();
        try {
            $Khipu = \Khipu::loadService('GetPaymentNotification');
            $Khipu->setParameters(["notification_token"=>$post["notification_token"]]);
            $response = json_decode($Khipu->consult(),true);
            $this->SaveLog($post,$response);

            $Khipu2 = \Khipu::loadService('PaymentStatus');
            $Khipu2->setParameters(["payment_id"=>$response["payment_id"]]);
            $response2 = json_decode($Khipu2->consult(),true);

            if(KhipuHistory::where("payment_id",$response["payment_id"])->first()["ready-for-terminal"]){
                return response()->json([
                    'status' => 'error'
                ],400);
            }

            $this->SaveLog($post,$response2);

            if($response2["status"]!="done"){
                return response()->json([
                    'status' => 'error'
                ],200);
            }

            $update = KhipuHistory::where("payment_id",$response["payment_id"])->update([
                "notification_token"=>$response["notification_token"],
                "receiver_id"=>$response["receiver_id"],
                "subject"=>$response["subject"],
                "amount"=>$response["amount"],
                "discount"=>$response["discount"],
                "custom"=>$response["custom"],
                "transaction_id"=>$response["transaction_id"],
                "currency"=>$response["currency"],
                "payer_email"=>$response["payer_email"],
                "payer_identifier"=>$response["payer_identifier"],
                "account_number"=>$response["account_number"],
                "bank_name"=>$response["bank_name"],
                "out_of_date_conciliation"=>$response["out_of_date_conciliation"],
                "conciliation_date"=>$response["conciliation_date"],
                "status"=>$response2["status"],
                "status_detail"=>$response2["detail"],
                "ready-for-terminal"=>true
            ]);

            if($update){
                return $this->DoTheOption(KhipuHistory::where("payment_id",$response["payment_id"])->first());
            }else{
                return response()->json([
                    'status' => 'error'
                ],500);
            }
        } catch (\Khipu\ApiException $e) {
            $save = $this->SaveLog($post,$response);
            return response()->json([
                'status' => 'error '.$e->getMessage()
            ],500);
        }

    }

    private function DoTheOption(KhipuHistory $data){
        try {
            $item = json_decode($data->item);
            if(count($item->sub_options)>0){
                foreach ($item->sub_options as $key => $value) {
                    switch ($value->detail) {
                        case 'update':
                            if($resp = $this->CreateUpdatePub($data->id_publication,$data->payment_id,new \DateTime(),1))return response()->json(['status' => 10,'detail'=>$resp],500);
                            $this->UpdatePubs();
                        break;
                        case 'update4weak':
                            $today = new \DateTime();
                            $today->modify('+1 week');
                            $this->CreateUpdatePub($data->id_publication,$data->payment_id,$today,1);
                            $today->modify('+1 week');
                            $this->CreateUpdatePub($data->id_publication,$data->payment_id,$today,1);
                            $today->modify('+1 week');
                            $this->CreateUpdatePub($data->id_publication,$data->payment_id,$today,1);
                            $today->modify('+1 week');
                            $this->CreateUpdatePub($data->id_publication,$data->payment_id,$today,1);
                        break;
                        case 'showcase':
                            $today = new \DateTime();
                            //$today->modify('+1 hour');
                            $today->modify('+1 month');
                            $this->CreateUpdatePub($data->id_publication,$data->payment_id,$today,2);
                            \Publications::where("id",$update->id_pub)->update([
                                "id_type_publications"=>3
                            ]);
                        break;
                        case 'featured':
                            \DB::table('featureds')->insert([
                                "name"=>"destacado",
                                "color"=>"",
                                "id_publication"=>$data->id_publication
                            ]);
                        break;
                        case '20photos':
                            $this->CreateUpdatePub($data->id_publication,$data->payment_id,$today,2);
                        break;
                        case 'all2':
                            $today = new \DateTime();
                            $today->modify('+1 month');
                            $this->CreateUpdatePub($data->id_publication,$data->payment_id,$today,2);
                            \Publications::where("id",$update->id_pub)->update(["id_type_publications"=>3]);   
                            $today2 = new \DateTime();
                            $today2->modify('+1 week');
                            $this->CreateUpdatePub($data->id_publication,$data->payment_id,$today2,1);
                            $today2->modify('+1 week');
                            $this->CreateUpdatePub($data->id_publication,$data->payment_id,$today2,1);
                            $today2->modify('+1 week');
                            $this->CreateUpdatePub($data->id_publication,$data->payment_id,$today2,1);
                            $today2->modify('+1 week');
                            $this->CreateUpdatePub($data->id_publication,$data->payment_id,$today2,1);
                            \DB::table('featureds')->insert([
                                "name"=>"destacado",
                                "color"=>"",
                                "id_publication"=>$data->id_publication
                            ]);
                        break;
                        default:
                            return response()->json([
                                'status' => 25,
                                'detail' => "detail pay not found"
                            ]);
                        break;
                    }
                }
            return response()->json(['status' => 0]);
            }
        } catch (\Throwable $th) {
            return response()->json(['status' => 10,'detail'=>$th->getMessage()],500);
        }
    }

    private function CreateUpdatePub($id_pub,$id_pay,$datetime,$type){
        try {
            \DB::table('updates')->insert([
                "type"=>$type,
                "id_pub"=>$id_pub,
                "id_pay"=>$id_pay,
                "date_execute"=>$datetime,
                "executed"=>0,
            ]);
            return false;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    private function SaveLog($request,$response){
        try {
            \DB::table("logs_khipu")->insert([
                "request"=>json_encode($request),
                "response"=>json_encode($response),
            ]);
            return true;
        } catch (\Khipu\ApiException $e) {
            return false;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function GetBanks(){
        //  Optener bancos
        try {
            $response = json_decode(\Khipu::loadService('ReceiverBanks')->consult());
            return response()->json([
                'status' => 'success',
                'banks' => $response
            ]);
        } catch (\Khipu\ApiException $e) {
            return response()->json([
                'status' => 'error',
                'detail' => $e->getResponseBody()
            ]);
        }
    }
    private function UpdatePubs(){
        $today = \Carbon\Carbon::now();
        $updates = \DB::table('updates')->where("date_execute","<=",$today)->where("executed",0)->get();
        foreach ($updates as $key => $update) {
            if($update->type==1){
                Publications::where("id",$update->id_pub)->update([
                    "order"=>$today
                ]);
                \DB::table('updates')->where("id",$update->id)->update([
                    "executed"=>1,
                    "date_executed"=>$today
                ]);
            }else if ($update->type==2){
                Publications::where("id",$update->id_pub)->update([
                    "id_type_publications"=>1
                ]);
                \DB::table('updates')->where("id",$update->id)->update([
                    "executed"=>1,
                    "date_executed"=>$today
                ]);
            }
        }
    }
}
