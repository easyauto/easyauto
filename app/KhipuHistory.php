<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KhipuHistory extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "subject",
        "currency",
        "amount",
        "id_publication",
        "notify_api_version",
        "bill-id",
        "url",
        "manual-url",
        "mobile-url",
        "ready-for-terminal",
        "payment_id",
        "item"
    ];
}
