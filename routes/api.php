<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// ROUTERS FOR LOGIN
Route::post('login', 'AuthController@login');
Route::post('SaveImageCache', 'PublicController@SaveImage');
Route::post('ChangeImagesNameCache', 'PublicationsController@changeNameImages');
// ROUTERS PUBLIC visitsAdd
Route::post('register', 'PublicController@register');
Route::post('visitsAdd', 'PublicationsController@visitsAdd');
Route::get('publications_search', 'PublicationsController@Regions');
Route::post('publications_search', 'PublicationsController@Search');
Route::get('publications_get', 'PublicController@index');
Route::get('publications_premium', 'PublicationsController@publications_premium');//
Route::get('update-pubs', 'PublicationsController@UpdatePubs');//
Route::get('publications/{id}/edit', 'PublicationsController@edit');//
Route::post('send-msj-to-pub','PublicController@SendMsjToPub');
Route::post('save-data-credit','PublicController@SaveDataCredit');
Route::post('save-data-credit-short','PublicController@SaveDataCredit2');
Route::get('delete-image/{imageName}','PublicController@DeleteImagen');

// INTEGRACION KHIPU
Route::get('khipu/banks','KhipuController@GetBanks');
Route::post('khipu/create-payment','KhipuController@CreatePayment');
Route::post('khipu/notify','KhipuController@GetPayment');

// ROUTERS PRIVATES
Route::resource('publications', 'PublicationsController');
Route::middleware("jwt.auth")->group(function(){
    
    Route::post('configuration-options/{id}', 'ConfigurationsController@updateOptions');
    Route::get('khipu/{id}', 'KhipuController@VerificPaymentById');
    Route::get('logout', 'AuthController@logout');
    Route::get('check', 'AuthController@validate_token');
    Route::put('update_user/{id}', 'PublicationsController@updateuser');
    Route::get('togglePub/{id}', 'PublicationsController@togglePublication');
    Route::post('UpdatePublicationType', 'PublicationsController@UpdatePublicationType');
    Route::get('GetPublicationAdmin', 'PublicationsController@GetPublicationAdmin');
    Route::post('create-upgrade-pub', 'PublicationsController@CreateUpdatePub');
});
